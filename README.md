# firebase-payment

These are Firebase Functions which integrate with payment providers.

Currently supported payment providers are Paypal and Stripe.

## Usage

In the main source file for your Firebase Functions:

```javascript
let context = {
  exports: exports,
  functions: require( 'firebase-functions' ),
  serviceAccount: require( './credentials/serviceAccount.json' )
};
let settings = {};

require( '@coldandgoji/firebase-payment` )( context )( settings );
```

## Environment Variables

Add the following in your Firebase Functions Config:

```
firebase functions:config:set \
localization.country.code="<Country Code> \
localization.currency="<Currency Code> \
localization.locale="<Locale Code (i.e. "en-AU")> \
localization.timezone="<Timezone Name (i.e. "Australia/Melbourne")> \
payment.paypal.live.client.id="<Paypal Live Client Id>" \
payment.paypal.live.client.secret="<Paypal Live Client Secret>" \
payment.paypal.sandbox.client.id="<Paypal Sandbox Client Id>" \
payment.paypal.sandbox.client.secret="<Paypal Sandbox Client Secret>" \
payment.stripe.live.apikey="<Stripe Live API Key>" \
payment.stripe.test.apikey="<Stripe Test API Key>"
```

These environment variables are required.

## Settings

Define your settings object as follows:

```javascript
let settings = {
  invoice: {
    id: { // Customer facing invoice ID: i.e., COMPANY-100023
      prefix: '<Prefix for client facing invoice IDs>',
      autonumber: <Number to start counting at for invoice IDs>
    }
  },
  paypal: {
    mode: '<"sandbox" or "live">',
    live: {
      webhook: { id: '<Paypal Webhook Id>' },
      experience: { id: '<Paypal Experience Id>' },
      url: {
        cancel: 'http://example.com/',
        return: 'http://example.com/'
      }
    },
    sandbox: {
      webhook: { id: '<Paypal Webhook Id>' },
      experience: { id: '<Paypal Experience Id>' },
      url: {
        cancel: 'http://example.com/',
        return: 'http://example.com/'
      }
    }
  },
  product: {
    attribute: {
      onsale: '<Contentful Entry Id>',
      addon: '<Contentful Entry Id>'
    }
  },
  sendgrid: {
    companyName: '<Name of Company used in email>',
    from: '<From address for transactional emails>',
    bcc: '<Comma separated BCCs for transaction emails>',
    template: { purchaseReceipt: '<Template ID for Purchase Receipt Emails>' }
  },
  stripe: { integration: '<"test" or "live">' }
};
```

These settings are required.
