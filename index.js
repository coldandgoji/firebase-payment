const _ = require( 'lodash' )

module.exports = ( context = {} ) => {
  return ( settings = {} ) => {
    context.exportName = 'payment'

    console.log( `Initializing "@coldandgoji/firebase-${context.exportName}"...` )

    // The only environment variables that should be used are those scoped to project.
    console.log( 'Loading environment...' )
    context.env = context.functions.config()[ context.exportName ]

    // Initializing and loading core library
    console.log( 'Loading "@coldandgoji/firebase"...' )
    context.core = require( '@coldandgoji/firebase' )( {
      functions: context.functions,
      serviceAccount: context.serviceAccount
    } )( settings )

    console.log( 'Loading local libraries...' )
    context.libs = require( './libs/index' )( context )( settings )

    console.log( 'Performing validation checks...' )
    context.libs.config.validate()
    context.libs.settings.validate()

    console.log( 'Loading source...' )
    let source = require( './source/index' )( context )( settings )

    console.log( 'Exporting triggers...' )
    let exports = context.exports
    _.each( _.keys( source ), trigger => {
      trigger === 'https' ? exports[ context.exportName ] = source[ trigger ]() : exports[ `${context.exportName}_${trigger}` ] = source[ trigger ]()
    } )

    console.log( `Initialization complete for "@coldandgoji/firebase-${context.exportName}".` )
  }
}
