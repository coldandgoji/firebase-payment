const _ = require( 'lodash' )
const HTTP = require( 'request-promise-native' )
const moment = require( 'moment' )
require( 'moment-timezone' )
const stringify = require( 'json-stringify-safe' )
const stripeAPI = require( 'stripe' )
const uuid = require( 'uuid/v4' )
const VError = require( 'verror' )

module.exports = ( context = {} ) => {
  return ( settings = {} ) => {
    // Expose name of export
    let exportName = context.exportName

    // Expose core libraries
    let core = context.core || require( '@coldandgoji/firebase' )( {
      functions: context.functions,
      serviceAccount: context.serviceAccount
    } )( settings )

    let cLibs = core.libs
    let cCart = cLibs.cart
    let cDatabase = cLibs.database
    let cError = cLibs.error
    let cInvoice = cLibs.invoice
    let cLog = cLibs.log
    let cProduct = cLibs.product
    let cUser = cLibs.user

    let cLocalization = cLibs.localization
    let countryCode = cLocalization.country.code()
    let currency = cLocalization.currency()
    let locale = cLocalization.locale()
    let timezone = cLocalization.timezone()

    // Expose enivronment variables
    let env = context.env || context.functions.config()[ exportName ]

    // Stripe libraries initialized on startup
    let stripe

    /*
     * Config
     * #config
     */

    let _config = {}

    _config.validate = ( params = {} ) => {
      let errors = []

      // Check localization
      if ( countryCode instanceof Error ) { errors.push( countryCode.message ) }
      if ( currency instanceof Error ) { errors.push( currency.message ) }
      if ( locale instanceof Error ) { errors.push( locale.message ) }
      if ( timezone instanceof Error ) { errors.push( timezone.message ) }
      // Only check the Paypal client id and secret if there is a valid mode.
      if ( !_.isError( paypalMode() ) ) {
        if ( paypalClientId() instanceof Error ) { errors.push( paypalClientId().message ) }
        if ( paypalClientSecret() instanceof Error ) { errors.push( paypalClientSecret().message ) }
      }
      if ( stripeApiKey() instanceof Error ) { errors.push( stripeApiKey().message ) }

      // Handle settings errors
      if ( errors.length ) {
        throw new VError( `Could not validate config object. These errors occurred: \n${errors.join( '\n' )}` )
      }
    }

    /*
     * Initialize
     * #initialize
     * This gets run before returning libraries.
     */

    let initialize = () => {
      stripe = stripeAPI( stripeApiKey() )
    }

    /*
     * Paypal
     * #paypal
     */
    let _paypal = {}

    /*
     * Paypal Access Token
     * #paypal-accessToken
     */

    _paypal.accessToken = {}

    let fetchPaypalAccessToken = () => {
      cLog( 'Fetching access token...' )

      let authorization = Buffer.from( `${paypalClientId()}:${paypalClientSecret()}` ).toString( 'base64' )

      return HTTP( {
        method: 'POST',
        uri: `https://${paypalURLHost()}/v1/oauth2/token`,
        headers: {
          Authorization: `Basic ${authorization}`,
          Accept: 'application/json',
          'Accept-Language': 'en_US'
        },
        resolveWithFullResponse: true,
        body: 'grant_type=client_credentials'
      } )

        .then( data => {
          let body = _.attempt( JSON.parse, data.body )

          if ( data.statusCode < 300 ) {
            cLog( `(${data.statusCode})`, 'Successfully retrieved access token from PayPal:', body )
            return body.access_token
          } else {
            cLog( `(${data.statusCode})`, 'Failed to retrieve access token from PayPal:', body )
            return formatPaypalError( {
              code: data.statusCode,
              body: body
            } )
          }
        } )
        .catch( error => {
          cLog( 'Failed to retrieve access token from PayPal:', error )
          return formatPaypalError( {
            code: error.statusCode,
            body: _.attempt( JSON.parse, _.result( error, 'response.body', '{}' ) )
          } )
        } )
    }
    _paypal.accessToken.fetch = fetchPaypalAccessToken

    /*
     * Paypal Capture
     * #paypal-capture
     */

    _paypal.capture = {}

    let fetchPaypalCapture = ( params = {} ) => {
      if ( params.accessToken ) {
        cLog( 'Fetching captured payment from PayPal...' )

        return HTTP( {
          method: 'GET',
          uri: `https://${paypalURLHost()}/v1/payments/capture/${params.captureId}`,
          headers: {
            Authorization: `Bearer ${params.accessToken}`,
            'Content-Type': 'application/json'
          },
          resolveWithFullResponse: true
        } )
          .then( data => {
            let body = _.attempt( JSON.parse, data.body )

            if ( data.statusCode < 300 ) {
              cLog( `(${data.statusCode})`, 'Successfully fetched captured payment from PayPal:', body )
              return { capture: body }
            } else {
              cLog( `(${data.statusCode})`, 'Failed to fetch captured payment from PayPal:', body )
              return formatPaypalError( {
                code: data.statusCode,
                body: body
              } )
            }
          } )
          .catch( error => {
            cLog( 'Failed to fetch captured payment from PayPal:', error )
            return formatPaypalError( {
              code: error.statusCode,
              body: _.attempt( JSON.parse, _.result( error, 'response.body', '{}' ) )
            } )
          } )
      } else {
        return fetchPaypalAccessToken()
          .then( accessToken => {
            if ( accessToken instanceof Error ) { return accessToken }
            params.accessToken = accessToken
            return fetchPaypalCapture( params )
          } )
      }
    }
    _paypal.capture.fetch = fetchPaypalCapture

    let refundPaypalCapture = ( params = {} ) => {
      if ( params.accessToken ) {
        cLog( 'Refunding PayPal capture...' )

        let payload = params.amount ? { amount: params.amount } : {}

        return HTTP( {
          method: 'POST',
          uri: `https://${paypalURLHost()}/v1/payments/capture/${params.captureId}/refund`,
          headers: {
            Authorization: `Bearer ${params.accessToken}`,
            'Content-Type': 'application/json'
          },
          resolveWithFullResponse: true,
          body: stringify( payload )
        } )
          .then( data => {
            let body = _.attempt( JSON.parse, data.body )

            if ( data.statusCode < 300 ) {
              cLog( `(${data.statusCode})`, 'Successfully refunded captured payment for PayPal:', body )
              return { capture: body }
            } else {
              cLog( `(${data.statusCode})`, 'Failed to refund captured payment for PayPal:', body )
              return formatPaypalError( {
                code: data.statusCode,
                body: body
              } )
            }
          } )
          .catch( error => {
            cLog( 'Failed to refund captured payment for PayPal:', error )
            return formatPaypalError( {
              code: error.statusCode,
              body: _.attempt( JSON.parse, _.result( error, 'response.body', '{}' ) )
            } )
          } )
      } else {
        return fetchPaypalAccessToken()
          .then( accessToken => {
            if ( accessToken instanceof Error ) { return accessToken }
            params.accessToken = accessToken
            return refundPaypalCapture( params )
          } )
      }
    }
    _paypal.capture.refund = refundPaypalCapture

    /*
     * Paypal Client
     * #paypal-client
     */

    _paypal.client = {}

    let paypalClientId = () => {
      let value = _.result( env, `paypal.${paypalMode()}.client.id`, null )
      if ( _.isString( value ) ) {
        return value
      } else {
        return new VError( `Invalid "${exportName}.paypal.${paypalMode()}.client.id" in Firebase Functions config object. (${value})` )
      }
    }
    _paypal.client.id = paypalClientId

    let paypalClientSecret = () => {
      let value = _.result( env, `paypal.${paypalMode()}.client.secret`, null )
      if ( _.isString( value ) ) {
        return value
      } else {
        return new VError( `Invalid "${exportName}.paypal.${paypalMode()}.client.secret" in Firebase Functions config object. (${value})` )
      }
    }
    _paypal.client.secret = paypalClientSecret

    /*
     * Paypal Error
     * #paypal-error
     * https://developer.paypal.com/docs/api/payments/#errors
     */

    _paypal.error = {}

    let formatPaypalError = ( params = {} ) => {
      cLog( 'Formatting PayPal error...', params )

      let code = params.code

      if ( code >= 500 ) { console.error( `(${code}) PayPal Error`, params ) }

      return cError.create( {
        info: {
          https: {
            code: code,
            response: {
              message: params.body.message,
              status: params.body.name,
              details: params.body.details
            }
          }
        },
        message: 'Could not complete PayPal operation.'
      } )
    }
    _paypal.error.format = formatPaypalError

    /*
     * Paypal Invoice
     * #paypal-invoice
     */

    _paypal.invoice = {}

    let createPaypalInvoice = ( params = {} ) => {
      cLog( 'Creating PayPal invoice...', params )

      let invoiceId = params.invoiceId
      let invoice
      let payment
      let notes
      let uid
      let billingId
      let shippingId
      let saleId

      try {
        _.each( params.payment.transactions[0].related_resources, resource => {
          if ( resource.sale ) { saleId = resource.sale.id }
        } )
      } catch ( error ) {
        console.error( 'Could not find PayPal Sale ID for transaction.' )
      }

      return fetchTransaction( invoiceId )

        .then( dataSnapshot => {
          if ( dataSnapshot instanceof Error ) { return dataSnapshot }

          let data = dataSnapshot.val()
          uid = data.user
          billingId = data.billingId
          shippingId = data.shippingId
          notes = data.notes
          payment = data.payment

          cLog( 'Fetched transaction:', data )

          return Promise.all( [
            cInvoice.items.format( { items: data.items } ),
            cUser.fetch( uid )
          ] )
        } )

        .catch( error => { return error } )

        .then( data => {
          data = cError.parse( data )
          if ( data instanceof Error ) { return data }

          let user = data[ 1 ].val()

          invoice = {
            processor: 'paypal',
            items: data[ 0 ],
            sale: { id: saleId },
            // TODO clean this up
            cost: paypalInvoiceCost( { resources: { payment: payment } } ),
            // TODO: replace with real logic
            status: {
              code: 'approved',
              message: ''
            },
            user: uid,
            email: user.email || user.contactEmail,
            phone: user.phone || user.contactPhone,
            address: {
              billing: _.result( user, `address.${billingId}`, {} ),
              shipping: _.result( user, `address.${shippingId}`, {} )
            },
            notes: notes
          }

          return cInvoice.id.create()
        } )

        .then( id => {
          if ( id instanceof Error ) { return id }

          invoice.id = id

          return cDatabase.set( {
            path: cInvoice.database.path( invoiceId ),
            value: invoice
          } )
        } )

        .then( reference => {
          // If storing locally fails, error with searchable string
          if ( reference instanceof Error ) {
            let error = new VError( {
              name: 'ABANDONED',
              cause: reference,
              info: {
                invoice: invoice
              }
            }, 'Could not add invoice to database.' )

            console.error( error )
            cLog( 'ABANDONDED: Invoice' )
            cLog( invoice )

            // Do not error out
            // Failing to log invoice is irrelevant to user
          }
          return invoice
        } )
    }
    _paypal.invoice.create = createPaypalInvoice

    let paypalInvoiceCost = ( params = {} ) => {
      cLog( 'Calculating PayPal invoice cost...', params )

      let resources = params.resources
      let cost = {}

      let payment = resources.payment || {}
      let paymentTransactions = payment.transactions || []
      let paymentCurrency
      let paymentTotal = 0

      _.each( paymentTransactions, transaction => {
        let amount = transaction.amount || {}
        paymentCurrency = paymentCurrency || amount.currency
        paymentTotal += amount.total
      } )

      cost.amount = _.floor( paymentTotal, 2 )
      cost.currency = paymentCurrency

      return cost
    }
    _paypal.invoice.cost = paypalInvoiceCost

    let paypalInvoiceStatus = ( params = {} ) => {
      cLog( 'Determining PayPal invoice status...', params )

      let status = { code: '', message: '' }
      let resources = params.resources || {}
      let capture = resources.capture || {}
      let payment = resources.payment || {}
      // let refund = resources.refund || {}
      let sale = resources.sale || {}

      // If a sale is completed, check to see if funds have been captured
      if ( sale.state === 'completed' ) {
        // Funds were captured. Set "captured" status
        if ( capture.state === 'completed' ) {
          status.code = 'captured'
        // Funds were not captured but sale was successfully completed. Set "approved" status
        } else {
          status.code = 'approved'
        }
      // Sale not completed. Defer to sale state
      } else if ( sale.state ) {
        status.code = sale.state
        // Payment was approved, but sale not yet available. Set "approved" state
      } else if ( payment.state === 'approved' ) {
        status.code = payment.state
        // Payment was created, but sale not yet available. Set "processing" state
      } else if ( payment.state === 'created' ) {
        status.code = 'processing'
        status.message = 'Payment has been created, but has not yet been approved.'
        // Payment has failed. Set "denied" state
      } else if ( payment.state === 'failed' ) {
        status.code = 'denied'
        status.message = `The following reasons have been cited for the denied payment: ${payment.failure_reason}`
        // Payment has a state but it is not valid. Set "invalid" state
      } else {
        status.code = 'invalid'
        status.message = 'There is no valid status for this invoice. Contact the administrator.'
      }

      return status
    }
    _paypal.invoice.status = paypalInvoiceStatus

    let updatePaypalInvoice = ( params = {} ) => {
      cLog( 'Updating PayPal invoice...', params )

      let resource = params.resource
      let resources = params.resources
      let rootPath = cInvoice.database.path( resource.parent_payment )

      return cDatabase.set( {
        path: `${rootPath}/status`,
        value: paypalInvoiceStatus( { resources: resources } )
      } )
        .then( data => {
          if ( data instanceof Error ) { return data }
          cLog( 'Successfully updated invoice.' )
          return data
        } )
    }
    _paypal.invoice.update = updatePaypalInvoice

    /*
     * Paypal Invoice Items
     * #paypal-invoice-items
     */

    _paypal.invoice.items = {}

    let formatPaypalInvoiceItems = ( params = {} ) => {
      cLog( 'Formatting PayPal invoice items...', params )

      let response = {}
      let items = params.items

      _.each( items, item => {
        response[ uuid() ] = {
          currency: item.currency,
          price: item.price,
          productId: item.sku,
          quantity: item.quantity
        }
      } )

      return response
    }
    _paypal.invoice.items.format = formatPaypalInvoiceItems

    /*
     * Paypal Item
     * #paypal-item
     * https://developer.paypal.com/docs/api/payments/#definition-item
     */

    _paypal.item = {}

    let createPaypalItem = ( params = {} ) => {
      cLog( 'Creating item...', params )
      let item = params.item

      return {
        name: item.product.name[ locale ],
        description: item.product.description[ locale ],
        quantity: params.quantity || item.quantity,
        price: paypalItemPrice( { item: item } ),
        sku: _.result( item, `product.modelNumber.${locale}`, item.productId ),
        currency: currency
      }
    }
    _paypal.item.create = createPaypalItem

    let paypalItemPrice = ( params = {} ) => {
      let item = params.item
      let product = item.product

      let attributesField = product.attributes || {}
      let attributes = attributesField[ locale ] || []

      let salePriceField = product.salePrice || {}
      let salePrice = salePriceField[ locale ]

      if ( salePrice && attributes.includes( cProduct.attribute.onsaleId() ) ) {
        return _.floor( salePrice, 2 )
      } else {
        return _.floor( product.price[ locale ], 2 )
      }
    }
    _paypal.item.price = paypalItemPrice

    /*
     * Paypal Item List
     * #paypal-item-list
     * https://developer.paypal.com/docs/api/payments/#definition-item_list
     */

    _paypal.itemList = {}

    let createPaypalItemList = ( params = {} ) => {
      cLog( 'Creating PayPal item list...', params )

      let items = params.items
      let itemList = { items: [] }

      return createPaypalShippingAddress( params )

        .then( shippingAddress => {
          itemList.shipping_address = shippingAddress

          _.each( items, item => {
            itemList.items.push( createPaypalItem( { item: item } ) )
            cLog( 'Addons:', item.addons )
            _.each( item.addons, addon => {
              cLog( 'Addon:', addon )
              itemList.items.push( createPaypalItem( { item: addon, quantity: item.quantity } ) )
            } )
          } )

          cLog( 'Item list:', stringify( itemList ) )
          return itemList
        } )
    }
    _paypal.itemList.create = createPaypalItemList

    /*
     * Paypal Mode
     * #paypal-mode
     */

    let paypalMode = () => {
      let value = _.result( settings, 'paypal.mode', null )
      if ( _.isString( value ) ) {
        return value
      } else {
        return new VError( `Invalid "paypal.mode" in settings object. (${value})` )
      }
    }
    _paypal.mode = paypalMode

    /*
     * Paypal Payment
     * #paypal-payment
     * https://developer.paypal.com/docs/integration/direct/express-checkout/integration-jsv4/advanced-payments-api/create-express-checkout-payments/
     * https://developer.paypal.com/docs/integration/direct/express-checkout/integration-jsv4/advanced-payments-api/execute-payments/
     */

    _paypal.payment = {}

    let createPaypalPayment = ( params = {} ) => {
      cLog( 'Creating PayPal Payment...', params )

      let promises = [ createPaypalTransaction( params ) ]

      let accessToken = params.accessToken
      if ( !accessToken ) { promises.push( fetchPaypalAccessToken() ) }

      return Promise.all( promises )

        .catch( error => { return error } )

        .then( data => {
          data = cError.parse( data )
          if ( data instanceof Error ) { return data }

          let transaction = data[ 0 ]
          accessToken = accessToken || data[ 1 ]

          let payload = {
            intent: 'sale',
            redirect_urls: {
              cancel_url: paypalCancelURL(),
              return_url: paypalReturnURL()
            },
            payer: { payment_method: 'paypal' },
            transactions: [ transaction ]
          }

          if ( paypalExperienceId() ) { payload.experience_profile_id = paypalExperienceId() }

          cLog( 'Payload:', payload )

          return HTTP( {
            method: 'POST',
            uri: `https://${paypalURLHost()}/v1/payments/payment`,
            headers: {
              Authorization: `Bearer ${accessToken}`,
              'Content-Type': 'application/json'
            },
            resolveWithFullResponse: true,
            body: stringify( payload )
          } )
        } )
        .then( data => {
          if ( data instanceof Error ) { return data }

          let body = _.attempt( JSON.parse, data.body )

          if ( data.statusCode < 300 ) {
            cLog( `(${data.statusCode})`, 'Successfully created payment with PayPal:', body )
            return body
          } else {
            cLog( `(${data.statusCode})`, 'Failed to create payment with PayPal:', body )
            return formatPaypalError( {
              code: data.statusCode,
              body: body
            } )
          }
        } )
        .catch( error => {
          cLog( 'Failed to create payment with PayPal:', error )
          return formatPaypalError( {
            code: error.statusCode,
            body: _.attempt( JSON.parse, _.result( error, 'response.body', '{}' ) )
          } )
        } )
    }
    _paypal.payment.create = createPaypalPayment

    let executePaypalPayment = ( params = {} ) => {
      if ( params.accessToken ) {
        cLog( 'Executing PayPal payment...', params )

        return HTTP( {
          method: 'POST',
          uri: `https://${paypalURLHost()}/v1/payments/payment/${params.paymentID}/execute/`,
          headers: {
            Authorization: `Bearer ${params.accessToken}`,
            'Content-Type': 'application/json'
          },
          resolveWithFullResponse: true,
          body: stringify( { payer_id: params.payerID } )
        } )
          .then( data => {
            let body = _.attempt( JSON.parse, data.body )

            if ( data.statusCode < 300 ) {
              cLog( `(${data.statusCode})`, 'Successfully executed payment with PayPal:', body )
              return body
            } else {
              cLog( `(${data.statusCode})`, 'Failed to execute payment with PayPal:', body )
              return formatPaypalError( {
                code: data.statusCode,
                body: body
              } )
            }
          } )
          .catch( error => {
            cLog( 'Failed to execute payment with PayPal:', error )
            return formatPaypalError( {
              code: error.statusCode,
              body: _.attempt( JSON.parse, _.result( error, 'response.body', '{}' ) )
            } )
          } )
      } else {
        return fetchPaypalAccessToken()
          .then( accessToken => {
            if ( accessToken instanceof Error ) { return accessToken }
            params.accessToken = accessToken
            return executePaypalPayment( params )
          } )
      }
    }
    _paypal.payment.execute = executePaypalPayment

    let fetchPaypalPayment = ( params = {} ) => {
      if ( params.accessToken ) {
        cLog( 'Fetching payment from PayPal...', params )

        return HTTP( {
          method: 'GET',
          uri: `https://${paypalURLHost()}/v1/payments/payment/${params.paymentId}`,
          headers: {
            Authorization: `Bearer ${params.accessToken}`,
            'Content-Type': 'application/json'
          },
          resolveWithFullResponse: true
        } )
          .then( data => {
            let body = _.attempt( JSON.parse, data.body )

            if ( data.statusCode < 300 ) {
              cLog( `(${data.statusCode})`, 'Successfully fetched payment from PayPal:', body )
              return { payment: body }
            } else {
              cLog( `(${data.statusCode})`, 'Failed to fetch payment from PayPal:', body )
              return formatPaypalError( {
                code: data.statusCode,
                body: body
              } )
            }
          } )
          .catch( error => {
            cLog( 'Failed to fetch payment from PayPal:', error )
            return formatPaypalError( {
              code: error.statusCode,
              body: _.attempt( JSON.parse, _.result( error, 'response.body', '{}' ) )
            } )
          } )
      } else {
        return fetchPaypalAccessToken()
          .then( accessToken => {
            if ( accessToken instanceof Error ) { return accessToken }
            params.accessToken = accessToken
            return fetchPaypalPayment( params )
          } )
      }
    }
    _paypal.payment.fetch = fetchPaypalPayment

    /*
     * Paypal Refund
     * #paypal-refund
     */

    _paypal.refund = {}

    let issuePaypalRefund = ( params = {} ) => {
      let amount = params.amount
      let invoice = params.invoice
      let paypal = invoice.billing.paypal

      let errors = []
      let errorName = 'issue-paypal-refund'

      // Determine if sale or payment is being refunded
      if ( paypal.capture ) {
        switch ( paypal.capture.state ) {
          case 'completed':
            cLog( 'Refunding completed captured payment...', params )
            return refundPaypalCapture( { amount: amount, captureId: paypal.capture.id } )
          case 'partially_refunded':
            cLog( 'Refunding partially refunded captured payment...', params )
            return refundPaypalCapture( { amount: amount, captureId: paypal.capture.id } )
          case 'pending':
            errors.push( new VError( { name: errorName }, 'Could not refund captured payment. Capture is pending.' ) )
            break
          case 'refunded':
            errors.push( new VError( { name: errorName }, 'Could not refund captured payment. Capture is already refunded.' ) )
            break
          case 'denied':
            errors.push( new VError( { name: errorName }, 'Could not refund captured payment. Capture is denied.' ) )
            break
          default:
            // Try to refund sale instead
            errors.push( new VError( `Could not refund captured payment. Invalid capture state: ${paypal.capture.state}` ) )
            break
        }
      }

      if ( paypal.sale ) {
        switch ( paypal.sale.state ) {
          case 'completed':
            cLog( 'Refunding completed sale...', params )
            return refundPaypalSale( { amount: amount, saleId: paypal.sale.id } )
          case 'partially_refunded':
            cLog( 'Refunding partially refunded sale...', params )
            return refundPaypalSale( { amount: amount, saleId: paypal.sale.id } )
          case 'pending':
            errors.push( new VError( { name: errorName }, 'Could not refund sale. Sale is pending.' ) )
            break
          case 'refunded':
            errors.push( new VError( { name: errorName }, 'Could not refund sale. Sale is already refunded.' ) )
            break
          case 'denied':
            errors.push( new VError( { name: errorName }, 'Could not refund sale. Sale is denied.' ) )
            break
          default:
            errors.push( new VError( `Could not refund sale. Invalid sale state: ${paypal.sale.state}` ) )
            break
        }
      }

      if ( errors.length === 0 ) { errors.push( new VError( { name: errorName }, 'Could not refund. No valid "capture" or "sale" object for invoice.' ) ) }

      return cError.create( {
        error: new VError.MultiError( errors ),
        info: {
          https: {
            code: 400,
            response: {
              message: 'Could not issue refund with PayPal.',
              status: 'paypal/invalid-request'
            }
          }
        }
      } )
    }
    _paypal.refund.issue = issuePaypalRefund

    let fetchPaypalRefund = ( params = {} ) => {
      if ( params.accessToken ) {
        cLog( 'Fetching refund from PayPal...', params )

        return HTTP( {
          method: 'GET',
          uri: `https://${paypalURLHost()}/v1/refunds/refund/${params.refundId}`,
          headers: {
            Authorization: `Bearer ${params.accessToken}`,
            'Content-Type': 'application/json'
          },
          resolveWithFullResponse: true
        } )
          .then( data => {
            let body = _.attempt( JSON.parse, data.body )

            if ( data.statusCode < 300 ) {
              cLog( `(${data.statusCode})`, 'Successfully fetched refund from PayPal:', body )
              return { refund: body }
            } else {
              cLog( `(${data.statusCode})`, 'Failed to fetch refund from PayPal:', body )
              return formatPaypalError( {
                code: data.statusCode,
                body: body
              } )
            }
          } )
          .catch( error => {
            cLog( 'Failed to fetch refund from PayPal:', error )
            return formatPaypalError( {
              code: error.statusCode,
              body: _.attempt( JSON.parse, _.result( error, 'response.body', '{}' ) )
            } )
          } )
      } else {
        return fetchPaypalAccessToken()
          .then( accessToken => {
            if ( accessToken instanceof Error ) { return accessToken }
            params.accessToken = accessToken
            return fetchPaypalRefund( params )
          } )
      }
    }
    _paypal.refund.fetch = fetchPaypalRefund

    /*
     * Paypal Resources
     * #paypal-resources
     */

    _paypal.resources = {}

    let fetchPaypalResources = ( params = {} ) => {
      cLog( 'Fetching PayPal resources...', params )
      let accessToken = params.accessToken
      let resource = params.resource
      let resourceType = params.resourceType

      let promises = []

      switch ( resourceType ) {
        case 'capture':
          promises.push( fetchPaypalCapture( { accessToken: accessToken, captureId: resource.id } ) )
          break
        case 'payment':
          promises.push( fetchPaypalPayment( { accessToken: accessToken, paymentId: resource.id } ) )
          break
        case 'refund':
          promises.push( fetchPaypalRefund( { accessToken: accessToken, refundId: resource.id } ) )
          break
        case 'sale':
          promises.push( fetchPaypalSale( { accessToken: accessToken, saleId: resource.id } ) )
          break
        default:
          cLog( 'Unhandled resource type:', resourceType )
          break
      }

      // Don't want to fetch resources multiple times
      // Cannot confirm all webhook configurations, so I am being safe
      if ( resource.capture_id && resourceType !== 'capture' ) { promises.push( fetchPaypalCapture( { accessToken: accessToken, captureId: resource.capture_id } ) ) }
      if ( resource.parent_payment && resourceType !== 'payment' ) { promises.push( fetchPaypalPayment( { accessToken: accessToken, paymentId: resource.parent_payment } ) ) }
      if ( resource.refund_id && resourceType !== 'refund' ) { promises.push( fetchPaypalRefund( { accessToken: accessToken, refundId: resource.refund_id } ) ) }
      if ( resource.sale_id && resourceType !== 'sale' ) { promises.push( fetchPaypalSale( { accessToken: accessToken, saleId: resource.sale_id } ) ) }

      return Promise.all( promises )

        .catch( error => { return error } )

        .then( data => {
          data = cError.parse( data )
          if ( data instanceof Error ) { return data }

          let resources = {}

          // Add indexed response objects to single object
          _.each( data, resource => { resources = _.assign( resources, resource ) } )

          return resources
        } )
    }
    _paypal.resources.fetch = fetchPaypalResources

    /*
     * Paypal Sale
     * #paypal-sale
     */

    _paypal.sale = {}

    let fetchPaypalSale = ( params = {} ) => {
      if ( params.accessToken ) {
        cLog( 'Fetching sale from PayPal...', params )

        return HTTP( {
          method: 'GET',
          uri: `https://${paypalURLHost()}/v1/payments/sale/${params.saleId}`,
          headers: {
            Authorization: `Bearer ${params.accessToken}`,
            'Content-Type': 'application/json'
          },
          resolveWithFullResponse: true
        } )
          .then( data => {
            let body = _.attempt( JSON.parse, data.body )

            if ( data.statusCode < 300 ) {
              cLog( `(${data.statusCode})`, 'Successfully fetched sale from PayPal:', body )
              return { sale: body }
            } else {
              cLog( `(${data.statusCode})`, 'Failed to fetch sale from PayPal:', body )
              return formatPaypalError( {
                code: data.statusCode,
                body: body
              } )
            }
          } )
          .catch( error => {
            cLog( 'Failed to fetch sale from PayPal:', error )
            return formatPaypalError( {
              code: error.statusCode,
              body: _.attempt( JSON.parse, _.result( error, 'response.body', '{}' ) )
            } )
          } )
      } else {
        return fetchPaypalAccessToken()
          .then( accessToken => {
            if ( accessToken instanceof Error ) { return accessToken }
            params.accessToken = accessToken
            return fetchPaypalSale( params )
          } )
      }
    }
    _paypal.sale.fetch = fetchPaypalSale

    let refundPaypalSale = ( params = {} ) => {
      if ( params.accessToken ) {
        cLog( 'Refunding sale with PayPal...', params )

        let payload = params.amount ? { amount: params.amount } : {}

        return HTTP( {
          method: 'POST',
          uri: `https://${paypalURLHost()}/v1/payments/sale/${params.saleId}/refund`,
          headers: {
            Authorization: `Bearer ${params.accessToken}`,
            'Content-Type': 'application/json'
          },
          resolveWithFullResponse: true,
          body: stringify( payload )
        } )
          .then( data => {
            let body = _.attempt( JSON.parse, data.body )

            if ( data.statusCode < 300 ) {
              cLog( `(${data.statusCode})`, 'Successfully refunded sale with PayPal:', body )
              return { sale: body }
            } else {
              cLog( `(${data.statusCode})`, 'Failed to refunded sale with PayPal:', body )
              return formatPaypalError( {
                code: data.statusCode,
                body: body
              } )
            }
          } )
          .catch( error => {
            cLog( 'Failed to refunded sale with PayPal:', error )
            return formatPaypalError( {
              code: error.statusCode,
              body: _.attempt( JSON.parse, _.result( error, 'response.body', '{}' ) )
            } )
          } )
      } else {
        return fetchPaypalAccessToken()
          .then( accessToken => {
            if ( accessToken instanceof Error ) { return accessToken }
            params.accessToken = accessToken
            return refundPaypalSale( params )
          } )
      }
    }
    _paypal.sale.refund = refundPaypalSale

    /*
     * Paypal Shipping Address
     * #paypal-shipping-address
     * https://developer.paypal.com/docs/api/payments/#definition-shipping_address
     * TODO: Country code in address. Is this still an issue?
     * https://developer.paypal.com/docs/integration/direct/rest/country-codes/
     */

    _paypal.shippingAddress = {}

    let createPaypalShippingAddress = ( params = {} ) => {
      cLog( 'Creating shipping address...', params )

      return cDatabase.fetch( { path: `user/${params.uid}/address/${params.shippingId}` } )
        .then( shippingSnapshot => {
          if ( shippingSnapshot instanceof Error ) { return shippingSnapshot }

          let shipping = shippingSnapshot.val()

          return {
            line1: shipping.line1,
            line2: shipping.line2,
            city: shipping.city,
            state: shipping.state,
            postal_code: shipping.postcode,
            country_code: shipping.countrycode || countryCode
          }
        } )
    }
    _paypal.shippingAddress.create = createPaypalShippingAddress

    /*
     * Paypal Transaction
     * #paypal-transaction
     */

    _paypal.transaction = {}

    let createPaypalTransaction = ( params = {} ) => {
      cLog( 'Creating PayPal transaction...', params )

      // Fetch cart items and create transaction
      return cCart.items.fetch( params.uid )

        .then( items => {
          params.items = items
          return createPaypalItemList( params )
        } )

        .then( itemList => {
          let total = _.sumBy( itemList.items, ( item ) => { return ( _.floor( item.price, 2 ) * Number( item.quantity ) ) } )

          return {
            amount: {
              currency: currency,
              total: _.floor( total, 2 )
            },
            item_list: itemList,
            note_to_payee: _.truncate( params.notes, { length: 255 } )
          }
        } )
    }
    _paypal.transaction.create = createPaypalTransaction

    /*
     * Paypal URL
     * #paypal-url
     */

    _paypal.url = {}

    let paypalURLHost = () => {
      if ( paypalMode() === 'live' ) {
        return 'api.paypal.com'
      } else {
        return 'api.sandbox.paypal.com'
      }
    }
    _paypal.url.host = paypalURLHost

    let paypalCancelURL = () => {
      let value = _.result( settings, `paypal.${paypalMode()}.url.cancel`, null )
      if ( _.isString( value ) ) {
        return value
      } else {
        return new VError( `Invalid "paypal.${paypalMode()}.url.cancel" in settings object. (${value})` )
      }
    }
    _paypal.url.cancel = paypalCancelURL

    let paypalReturnURL = () => {
      let value = _.result( settings, `paypal.${paypalMode()}.url.return`, null )
      if ( _.isString( value ) ) {
        return value
      } else {
        return new VError( `Invalid "paypal.${paypalMode()}.url.return" in settings object. (${value})` )
      }
    }
    _paypal.url.return = paypalReturnURL

    /*
     * Paypal Experience
     * #paypal-experience
     */

    _paypal.experience = {}

    let paypalExperienceId = () => {
      let value = _.result( settings, `paypal.${paypalMode()}.experience.id`, null )
      if ( _.isString( value ) ) {
        return value
      } else {
        return new VError( `Invalid "paypal.${paypalMode()}.experience.id" in settings object. (${value})` )
      }
    }
    _paypal.experience.id = paypalExperienceId()

    let createPaypalExperience = ( params = {} ) => {
      if ( params.accessToken ) {
        cLog( 'Creating PayPal Experience...', params )

        return HTTP( {
          method: 'POST',
          uri: `https://${paypalURLHost()}/v1/payment-experience/web-profiles/`,
          headers: {
            Authorization: `Bearer ${params.accessToken}`,
            'Content-Type': 'application/json'
          },
          resolveWithFullResponse: true,
          body: stringify( {
            name: params.name,
            temporary: params.temporary || false,
            flow_config: params.flow_config,
            input_fields: params.input_fields,
            presentation: params.presentation
          } )
        } )
          .then( data => {
            let body = _.attempt( JSON.parse, data.body )

            if ( data.statusCode < 300 ) {
              cLog( `(${data.statusCode})`, 'Successfully created experience for PayPal:', body )
              return body
            } else {
              cLog( `(${data.statusCode})`, 'Failed to create experience for PayPal:', body )
              return formatPaypalError( {
                code: data.statusCode,
                body: body
              } )
            }
          } )
          .catch( error => {
            cLog( 'Failed to create experience for PayPal:', error )
            return formatPaypalError( {
              code: error.statusCode,
              body: _.attempt( JSON.parse, _.result( error, 'response.body', '{}' ) )
            } )
          } )
      } else {
        return fetchPaypalAccessToken()
          .then( accessToken => {
            if ( accessToken instanceof Error ) { return accessToken }
            params.accessToken = accessToken
            return createPaypalExperience( params )
          } )
      }
    }
    _paypal.experience.create = createPaypalExperience

    let listPaypalExperiences = ( params = {} ) => {
      if ( params.accessToken ) {
        cLog( 'Retrieveing list of PayPal Experiences...', params )

        return HTTP( {
          method: 'GET',
          uri: `https://${paypalURLHost()}/v1/payment-experience/web-profiles/`,
          headers: {
            Authorization: `Bearer ${params.accessToken}`,
            'Content-Type': 'application/json'
          },
          resolveWithFullResponse: true
        } )
          .then( data => {
            let body = _.attempt( JSON.parse, data.body )

            if ( data.statusCode < 300 ) {
              cLog( `(${data.statusCode})`, 'Successfully retrieved list of experiences from PayPal:', body )
              return body
            } else {
              cLog( `(${data.statusCode})`, 'Failed to retrieve list of experiences from PayPal:', body )
              return formatPaypalError( {
                code: data.statusCode,
                body: body
              } )
            }
          } )
          .catch( error => {
            cLog( 'Failed to retrieve list of experiences from PayPal:', error )
            return formatPaypalError( {
              code: error.statusCode,
              body: _.attempt( JSON.parse, _.result( error, 'response.body', '{}' ) )
            } )
          } )
      } else {
        return fetchPaypalAccessToken()
          .then( accessToken => {
            if ( accessToken instanceof Error ) { return accessToken }
            params.accessToken = accessToken
            return listPaypalExperiences( params )
          } )
      }
    }
    _paypal.experience.list = listPaypalExperiences

    let fetchPaypalExperience = ( params = {} ) => {
      if ( params.accessToken ) {
        cLog( 'Retrieveing PayPal Experience...', params )

        return HTTP( {
          method: 'GET',
          uri: `https://${paypalURLHost}/v1/payment-experience/web-profiles/${params.profileId}`,
          headers: {
            Authorization: `Bearer ${params.accessToken}`,
            'Content-Type': 'application/json'
          },
          resolveWithFullResponse: true
        } )
          .then( data => {
            let body = _.attempt( JSON.parse, data.body )

            if ( data.statusCode < 300 ) {
              cLog( `(${data.statusCode})`, 'Successfully retrieved experience from PayPal:', body )
              return body
            } else {
              cLog( `(${data.statusCode})`, 'Failed to retrieve experience from PayPal:', body )
              return formatPaypalError( {
                code: data.statusCode,
                body: body
              } )
            }
          } )
          .catch( error => {
            cLog( 'Failed to retrieve experience from PayPal:', error )
            return formatPaypalError( {
              code: error.statusCode,
              body: _.attempt( JSON.parse, _.result( error, 'response.body', '{}' ) )
            } )
          } )
      } else {
        return fetchPaypalAccessToken()
          .then( accessToken => {
            if ( accessToken instanceof Error ) { return accessToken }
            params.accessToken = accessToken
            return fetchPaypalExperience( params )
          } )
      }
    }
    _paypal.experience.fetch = fetchPaypalExperience

    let deletePaypalExperience = ( params = {} ) => {
      if ( params.accessToken ) {
        cLog( 'Deleting PayPal Experience...', params )

        return HTTP( {
          method: 'DELETE',
          uri: `https://${paypalURLHost()}/v1/payment-experience/web-profiles/${params.profileId}`,
          headers: {
            Authorization: `Bearer ${params.accessToken}`,
            'Content-Type': 'application/json'
          },
          resolveWithFullResponse: true
        } )
          .then( data => {
            let body = _.attempt( JSON.parse, data.body )

            if ( data.statusCode < 300 ) {
              cLog( `(${data.statusCode})`, 'Successfully deleted experience from PayPal:', body )
              return body
            } else {
              cLog( `(${data.statusCode})`, 'Failed to delete experience from PayPal:', body )
              return formatPaypalError( {
                code: data.statusCode,
                body: body
              } )
            }
          } )
          .catch( error => {
            cLog( 'Failed to delete experience from PayPal:', error )
            return formatPaypalError( {
              code: error.statusCode,
              body: _.attempt( JSON.parse, _.result( error, 'response.body', '{}' ) )
            } )
          } )
      } else {
        return fetchPaypalAccessToken()
          .then( accessToken => {
            if ( accessToken instanceof Error ) { return accessToken }
            params.accessToken = accessToken
            return deletePaypalExperience( params )
          } )
      }
    }
    _paypal.experience.delete = deletePaypalExperience

    let overwritePaypalExperience = ( params = {} ) => {
      if ( params.accessToken ) {
        cLog( 'Overwriting PayPal Experience...', params )

        return HTTP( {
          method: 'PUT',
          uri: `https://${paypalURLHost()}/v1/payment-experience/web-profiles/${params.profileId}`,
          headers: {
            Authorization: `Bearer ${params.accessToken}`,
            'Content-Type': 'application/json'
          },
          resolveWithFullResponse: true,
          body: stringify( {
            name: params.name,
            temporary: params.temporary || false,
            flow_config: params.flow_config,
            input_fields: params.input_fields,
            presentation: params.presentation
          } )
        } )
          .then( data => {
            let body = _.attempt( JSON.parse, data.body )

            if ( data.statusCode < 300 ) {
              cLog( `(${data.statusCode})`, 'Successfully overwrote experience for PayPal:', body )
              return body
            } else {
              cLog( `(${data.statusCode})`, 'Failed to  overwrite experience for PayPal:', body )
              return formatPaypalError( {
                code: data.statusCode,
                body: body
              } )
            }
          } )
          .catch( error => {
            cLog( 'Failed to  overwrite experience for PayPal:', error )
            return formatPaypalError( {
              code: error.statusCode,
              body: _.attempt( JSON.parse, _.result( error, 'response.body', '{}' ) )
            } )
          } )
      } else {
        return fetchPaypalAccessToken()
          .then( accessToken => {
            if ( accessToken instanceof Error ) { return accessToken }
            params.accessToken = accessToken
            return overwritePaypalExperience( params )
          } )
      }
    }
    _paypal.experience.overwrite = overwritePaypalExperience

    let patchPaypalExperience = ( params = {} ) => {
      if ( params.accessToken ) {
        cLog( 'Patching PayPal Experience...', params )

        return HTTP( {
          method: 'PATCH',
          uri: `https://${paypalURLHost()}/v1/payment-experience/web-profiles/${params.profileId}`,
          headers: {
            Authorization: `Bearer ${params.accessToken}`,
            'Content-Type': 'application/json'
          },
          resolveWithFullResponse: true,
          body: stringify( params.patch )
        } )
          .then( data => {
            let body = _.attempt( JSON.parse, data.body )

            if ( data.statusCode < 300 ) {
              cLog( `(${data.statusCode})`, 'Successfully patched experience for PayPal:', body )
              return body
            } else {
              cLog( `(${data.statusCode})`, 'Failed to patch experience for PayPal:', body )
              return formatPaypalError( {
                code: data.statusCode,
                body: body
              } )
            }
          } )
          .catch( error => {
            cLog( 'Failed to patch experience for PayPal:', error )
            return formatPaypalError( {
              code: error.statusCode,
              body: _.attempt( JSON.parse, _.result( error, 'response.body', '{}' ) )
            } )
          } )
      } else {
        return fetchPaypalAccessToken()
          .then( accessToken => {
            if ( accessToken instanceof Error ) { return accessToken }
            params.accessToken = accessToken
            return patchPaypalExperience( params )
          } )
      }
    }
    _paypal.experience.patch = patchPaypalExperience

    /*
     * Paypal Webhook
     * #paypal-webhook
     */

    _paypal.webhook = {}

    let paypalWebhookId = () => {
      let value = _.result( settings, `paypal.${paypalMode()}.webhook.id`, null )
      if ( _.isString( value ) ) {
        return value
      } else {
        return new VError( `Invalid "paypal.${paypalMode()}.webhook.id" in settings object. (${value})` )
      }
    }
    _paypal.webhook.id = paypalWebhookId

    let verifyPaypalWebhook = ( params = {} ) => {
      if ( params.accessToken ) {
        cLog( 'Verifying webhook with PayPal...', params )

        return HTTP( {
          method: 'POST',
          uri: `https://${paypalURLHost()}/v1/notifications/verify-webhook-signature`,
          headers: {
            Authorization: `Bearer ${params.accessToken}`,
            'Content-Type': 'application/json'
          },
          resolveWithFullResponse: true,
          body: stringify( {
            auth_algo: params.auth_algo,
            cert_url: params.cert_url,
            transmission_id: params.transmission_id,
            transmission_sig: params.transmission_sig,
            transmission_time: params.transmission_time,
            webhook_id: paypalWebhookId(),
            webhook_event: params.body
          } )
        } )
          .then( data => {
            let body = _.attempt( JSON.parse, data.body )

            if ( body.verification_status === 'SUCCESS' ) {
              cLog( `(${data.statusCode})`, 'Successfully verified webhook with PayPal:', body )
              return body
            } else {
              cLog( `(${data.statusCode})`, 'Failed to verify webhook with PayPal:', body )
              return formatPaypalError( {
                code: data.statusCode,
                body: body
              } )
            }
          } )
          .catch( error => {
            cLog( 'Failed to verify webhook with PayPal:', error )
            return formatPaypalError( {
              code: error.statusCode,
              body: _.attempt( JSON.parse, _.result( error, 'response.body', '{}' ) )
            } )
          } )
      } else {
        return fetchPaypalAccessToken()
          .then( accessToken => {
            if ( accessToken instanceof Error ) { return accessToken }
            params.accessToken = accessToken
            return verifyPaypalWebhook( params )
          } )
      }
    }
    _paypal.webhook.verify = verifyPaypalWebhook

    /*
     * Report
     * #report
     */

    let _report = {}

    /*
     * Report Database
     * #report-database
     */

    _report.database = {}

    let reportDatabasePath = ( params = {} ) => {
      let date = params.date
      let year = moment( date ).year()
      // moment indexes months from 0 to 11
      let month = moment( date ).month() + 1
      return `report/${year}/${month}`
    }
    _report.database.path = reportDatabasePath

    /*
     * Sendgrid
     * #sendgrid
     */

    let _sendgrid = {}

    let sendgridBcc = () => { return _.result( settings, 'sendgrid.bcc', '' ) }
    _sendgrid.bcc = sendgridBcc

    let sendgridCompanyName = () => {
      let value = _.result( settings, 'sendgrid.companyName', null )
      if ( _.isString( value ) ) {
        return value
      } else {
        return new VError( `Invalid "sendgrid.companyName" in settings object. (${value})` )
      }
    }
    _sendgrid.companyName = sendgridCompanyName

    let sendgridFrom = () => {
      let value = _.result( settings, 'sendgrid.from', null )
      if ( _.isString( value ) ) {
        return value
      } else {
        return new VError( `Invalid "sendgrid.from" in settings object. (${value})` )
      }
    }
    _sendgrid.from = sendgridFrom

    /*
     * Sendgrid Template
     * #sendgrid-template
     */

    _sendgrid.template = {}

    let sendgridPurchaseReceiptTemplate = () => {
      let value = _.result( settings, 'sendgrid.template.purchaseReceipt', null )
      if ( _.isString( value ) ) {
        return value
      } else {
        return new VError( `Invalid "sendgrid.template.purchaseReceipt" in settings object. (${value})` )
      }
    }
    _sendgrid.template.purchaseReceipt = sendgridPurchaseReceiptTemplate

    /*
     * Settings
     * #settings
     */

    let _settings = {}

    _settings.validate = ( params = {} ) => {
      let errors = []

      // Check invoice
      if ( cInvoice.id.autonumber() instanceof Error ) { errors.push( cInvoice.id.autonumber().message ) }
      if ( cInvoice.id.prefix() instanceof Error ) { errors.push( cInvoice.id.prefix().message ) }
      // Check paypal
      if ( paypalMode() instanceof Error ) { errors.push( paypalMode().message ) }
      if ( paypalCancelURL() instanceof Error ) { errors.push( paypalCancelURL().message ) }
      if ( paypalReturnURL() instanceof Error ) { errors.push( paypalReturnURL().message ) }
      if ( paypalWebhookId() instanceof Error ) { errors.push( paypalWebhookId().message ) }
      // Check stripe
      if ( stripeIntegration() instanceof Error ) { errors.push( stripeIntegration().message ) }
      // Check sendgrid
      if ( sendgridFrom() instanceof Error ) { errors.push( sendgridFrom().message ) }
      if ( sendgridCompanyName() instanceof Error ) { errors.push( sendgridCompanyName().message ) }
      if ( sendgridPurchaseReceiptTemplate() instanceof Error ) { errors.push( sendgridPurchaseReceiptTemplate().message ) }

      // Handle settings errors
      if ( errors.length ) {
        throw new VError( `Could not validate settings object. These errors occurred: \n${errors.join( '\n' )}` )
      }
    }

    /*
     * Stripe
     * #stripe
     */
    let _stripe = {}

    /*
     * Stripe Api Keys
     * #stripe-apiKey
     */

    _stripe.apiKey = {}

    let stripeApiKey = () => {
      let value = _.result( env, `stripe.${stripeIntegration()}.apikey`, null )
      if ( _.isString( value ) ) {
        return value
      } else {
        return new VError( `Invalid "${exportName}.stripe.${stripeIntegration()}.apikey" in Firebase Functions config object. (${value})` )
      }
    }
    _stripe.apiKey = stripeApiKey

    /*
     * Stripe Charge
     * #stripe-charge
     */

    _stripe.charge = {}

    let createStripeCharge = ( params = {} ) => {
      cLog( 'Creating Stripe charge...', params )

      let payload = {
        amount: _.floor( convertToStripePrice( { price: params.amount } ), 2 ),
        currency: currency
      }

      if ( params.source.card.id ) {
        // Charge customer with saved card
        payload.customer = params.customerId
        payload.source = params.source.card.id
      } else {
        // Charge no customer with card token
        payload.source = params.source.card.token
      }

      cLog( 'Payload:', payload )

      return stripe.charges.create( payload )
        .then( charge => {
          cLog( 'Successfully created charge with Stripe', charge )
          return charge
        }, error => {
          cLog( 'Failed to create charge with Stripe', error )
          return formatStripeError( { error: error } )
        } )
    }
    _stripe.charge.create = createStripeCharge

    let retrieveStripeCharge = ( params = {} ) => {
      cLog( 'Retrieving Stripe charge...', params )

      return stripe.charges.retrieve( params.chargeId )
        .then( charge => {
          cLog( 'Successfully retrieved charge from Stripe', charge )
          return charge
        }, error => {
          cLog( 'Failed to retrieve charge from Stripe', error )
          return formatStripeError( { error: error } )
        } )
    }
    _stripe.charge.retrieve = retrieveStripeCharge

    /*
     * Stripe Customer
     * #stripe-customer
     */

    _stripe.customer = {}

    let createStripeCustomer = ( params = {} ) => {
      cLog( 'Creating Stripe customer...', params )

      return stripe.customers.create( params )
        .then( customer => {
          cLog( 'Successfully created customer with Stripe', customer )
          return customer
        }, error => {
          cLog( 'Failed to create customer with Stripe', error )
          return formatStripeError( { error: error } )
        } )
    }
    _stripe.customer.create = createStripeCustomer

    let createStripeCustomerForUser = ( params = {} ) => {
      cLog( 'Creating Stripe customer for a user...', params )

      return createStripeCustomer( {
        email: params.email,
        metadata: { uid: params.uid }
      } )
        .then( customer => {
          if ( customer instanceof Error ) { return customer }

          return cDatabase.set( {
            path: cUser.database.path( params.uid ) + '/stripe',
            value: customer.id
          } )
            .then( result => {
              if ( result instanceof Error ) { console.error( 'Failed to assign Stripe customer to user:', result ) }
              return customer
            } )
        } )
    }
    _stripe.customer.createForUser = createStripeCustomerForUser

    let retrieveStripeCustomer = ( params = {} ) => {
      cLog( 'Retrieving Stripe customer...', params )

      return stripe.customers.retrieve( params.user.stripe )
        .then( customer => {
          cLog( 'Successfully retrieved customer from Stripe', customer )
          return customer
        }, error => {
          cLog( 'Failed to retrieve customer from Stripe', error )
          return formatStripeError( { error: error } )
        } )
    }
    _stripe.customer.retrieve = retrieveStripeCustomer

    let updateStripeCustomer = ( params = {} ) => {
      cLog( 'Updating Stripe customer...', params )

      return stripe.customers.update( params.user.stripe, params.body )
        .then( customer => {
          cLog( 'Successfully updated customer with Stripe', customer )
          return customer
        }, error => {
          cLog( 'Failed to update customer with Stripe', error )
          return formatStripeError( { error: error } )
        } )
    }
    _stripe.customer.update = updateStripeCustomer

    /*
     * Stripe Customer Source
     * #stripe-customer-source
     */

    _stripe.customer.source = {}

    let deleteStripeCustomerSource = ( params = {} ) => {
      cLog( 'Deleting Stripe source...', params )

      return stripe.customers.deleteSource( params.user.stripe, params.sourceId )
        .then( source => {
          cLog( 'Successfully deleted source with Stripe', source )
          return source
        }, error => {
          cLog( 'Failed to delete source with Stripe', error )
          return formatStripeError( { error: error } )
        } )
    }
    _stripe.customer.source.delete = deleteStripeCustomerSource

    let createStripeCustomerSource = ( params = {} ) => {
      cLog( 'Creating Stripe source...', params )

      return stripe.customers.createSource( params.user.stripe, { source: params.body.source } )
        .then( source => {
          cLog( 'Successfully created source with Stripe', source )
          return source
        }, error => {
          cLog( 'Failed to create source with Stripe', error )
          return formatStripeError( { error: error } )
        } )
    }
    _stripe.customer.source.create = createStripeCustomerSource

    /*
     * Stripe Dispute
     * #stripe-dispute
     */

    _stripe.dispute = {}

    let retrieveStripeDispute = ( params = {} ) => {
      cLog( 'Retrieving Stripe dispute...', params )

      return stripe.disputes.retrieve( params.disputeId )
        .then( dispute => {
          cLog( 'Successfully retrieved dispute from Stripe', dispute )
          return dispute
        }, error => {
          cLog( 'Failed to retrieve dispute from Stripe', error )
          return formatStripeError( { error: error } )
        } )
    }
    _stripe.dispute.retrieve = retrieveStripeDispute

    /*
     * Stripe Error
     * #stripe-error
     * https://stripe.com/docs/api#errors
       * error = {
       *   // Required
       *   type: '',
       *   charge: '',
       *   // Optional
       *   param: '',
       *   message: '',
       *   // Optional - Card Errors
       *   code: '',
       *   decline_code: ''
       * }
     */

    _stripe.error = {}

    let formatStripeError = ( params = {} ) => {
      cLog( 'Formatting Stripe error...', params )

      let stripeError = params.error
      let code = _.result( stripeError, 'statusCode', 400 )

      if ( code >= 500 ) { console.error( `(${code}) Stripe Error`, stripeError ) }

      let error = new VError( { name: stripeError.type }, stripeError.message )
      return cError.create( {
        error: error,
        info: {
          https: {
            code: code,
            response: {
              message: stripeError.message,
              status: stripeError.type
            }
          }
        },
        message: 'Could not complete Stripe operation.'
      } )
    }
    _stripe.error.format = formatStripeError

    /*
     * Stripe Integration
     * #stripe-integration
     */

    let stripeIntegration = () => {
      let value = _.result( settings, 'stripe.integration', null )
      if ( _.isString( value ) ) {
        return value
      } else {
        return new VError( `Invalid "stripe.integration" in settings object. (${value})` )
      }
    }
    _stripe.integration = stripeIntegration

    /*
     * Stripe Invoice
     * #stripe-invoice
     */

    _stripe.invoice = {}

    let createStripeInvoice = ( params = {} ) => {
      cLog( 'Creating invoice for Stripe charge...', params )

      let charge = params.charge
      let uid = params.uid

      let indexedCharge = {}
      indexedCharge[ charge.id ] = charge
      let invoice

      return cUser.fetch( uid )

        .then( userSnapshot => {
          let user
          if ( userSnapshot instanceof Error ) {
            console.log( 'Problem fetching user:', uid )
            user = {}
          } else {
            user = userSnapshot.val()
          }

          invoice = {
            processor: 'stripe',
            charge: { id: charge.id },
            card: charge.source,
            items: params.items,
            cost: stripeInvoiceCost( { charge: charge } ),
            status: stripeInvoiceStatus( { charge: charge } ),
            user: uid,
            email: user.email || user.contactEmail,
            phone: user.phone || user.contactPhone,
            address: {
              billing: _.result( user, `address.${params.billingId}`, {} ),
              shipping: _.result( user, `address.${params.shippingId}`, {} )
            },
            notes: params.notes
          }

          return cInvoice.id.create()
        } )

        .then( invoiceId => {
          if ( invoiceId instanceof Error ) { return invoiceId }

          invoice.id = invoiceId

          return cDatabase.set( {
            path: cInvoice.database.path( charge.id ),
            value: invoice
          } )
        } )

        .then( reference => {
          // If storing locally fails, error with searchable string
          if ( reference instanceof Error ) {
            let error = new VError( {
              name: 'ABANDONED',
              cause: reference,
              info: {
                invoice: invoice
              }
            }, 'Could not add invoice to database.' )

            console.error( error )
            cLog( 'ABANDONDED: Invoice' )
            cLog( invoice )

            // Do not error out
            // Failing to log invoice is irrelevant to user
          }

          return invoice
        } )
    }
    _stripe.invoice.create = createStripeInvoice

    let stripeInvoiceCost = ( params = {} ) => {
      let charge = params.charge || {}
      let amount = charge.amount / 100

      return {
        amount: _.floor( amount, 2 ),
        currency: charge.currency.toUpperCase()
      }
    }
    _stripe.invoice.cost = stripeInvoiceCost

    let stripeInvoiceStatus = ( params = {} ) => {
      cLog( 'Determining Stripe invoice status...', params )

      let status = { code: '', message: '' }

      let charge = params.charge

      if ( charge.dispute ) {
        status.code = 'disputed'
      } else if ( charge.refunded || charge.amount_refunded > 0 ) {
        if ( charge.amount === charge.amount_refunded ) {
          status.code = 'partially_refunded'
        } else {
          status.code = 'refunded'
        }
      } else if ( charge.captured ) {
        status.code = 'captured'
      } else if ( charge.failure_code ) {
        status.code = 'denied'
      } else if ( charge.paid ) {
        status.code = 'approved'
      } else if ( charge.id ) {
        status.code = 'pending'
      } else {
        status.code = 'invalid'
        status.message = 'There is no valid status for this invoice. Contact the administrator.'
      }

      return status
    }
    _stripe.invoice.status = stripeInvoiceStatus

    let updateStripeInvoiceStatus = ( params = {} ) => {
      cLog( 'Updating Stripe invoice status...', params )

      let status = { code: '', message: '' }

      let resources = params.resources || {}
      let charge = resources.charge || {}
      let dispute = resources.dispute || {}
      let refunds = resources.refunds || {}

      if ( charge.failure_code ) {
        status.code = 'denied'
        status.message = charge.failure_message
      } else if ( charge.dispute ) {
        status.code = 'disputed'
        if ( dispute.reason ) { status.message += `The reason is listed as: ${dispute.reason}.` }
        if ( dispute.status ) { status.message += `The status is listed as: ${dispute.status}.` }
      } else if ( charge.refunded ) {
        status.code = 'refunded'

        let reasons = _.join( _.map( refunds, refund => {
          if ( refund.reason ) {
            return refund.reason
          }
        } ), ' ' )
        let statuses = _.join( _.map( refunds, refund => {
          if ( refund.status ) {
            return refund.status
          }
        } ), ', ' )

        // There can be multiple refunds
        if ( _.keys( refunds ).length > 1 ) {
          status.message += `The reasons are listed as: ${reasons}. `
          status.message += `The statuses are listed as: ${statuses}`
        } else {
          status.message += `The reason is listed as: ${reasons}. `
          status.message += `The status is listed as: ${statuses}`
        }
      } else if ( charge.amount_refunded > 0 ) {
        status.code = 'partially_refunded'
        // TODO: You can make this more durable
        status.message = `The charge has been refunded by $${convertFromStripePrice( { price: charge.amount_refunded } )}.`
      } else if ( charge.captured ) {
        status.code = 'captured'
      } else if ( charge.paid ) {
        status.code = 'approved'
      } else if ( charge.id ) {
        status.code = 'processing'
      } else {
        status.code = 'invalid'
        status.message = 'There is no valid status for this invoice. Contact the administrator.'
      }

      let path = cInvoice.database.path( charge.id ) + '/status'

      return cDatabase.set( { path: path, value: status } )
    }
    _stripe.invoice.status.update = updateStripeInvoiceStatus

    /*
     * Stripe Invoice Charge
     * #stripe-invoice-charge
     */

    _stripe.invoice.charge = {}

    let stripeInvoiceChargeId = ( params = {} ) => { return params.invoice.stripe.charge.id }
    _stripe.invoice.charge.id = stripeInvoiceChargeId

    let updateStripeInvoiceCharge = ( params = {} ) => {
      cLog( 'Updating Stripe invoice charge...', params )

      let body = params.body
      let invoiceId = stripeWebhookInvoiceId( { body: body } )
      let chargeId = stripeWebhookChargeId( { body: body } )

      return retrieveStripeCharge( { chargeId: chargeId } )
        .then( charge => {
          if ( charge instanceof Error ) { return charge }

          let rootPath = cInvoice.database.path( invoiceId )

          return cDatabase.set( {
            path: `${rootPath}/billing/stripe/charge/${chargeId}`,
            value: charge
          } )
            .then( data => {
              if ( data instanceof Error ) {
                cLog( 'Failed to update Stripe invoice charge:', data )
                return data
              }

              cLog( 'Successfully updated Stripe invoice charge:', data )
              return { charge: charge }
            } )
        } )
    }
    _stripe.invoice.charge.update = updateStripeInvoiceCharge

    /*
     * Stripe Invoice Dispute
     * #stripe-invoice-dispute
     */

    _stripe.invoice.dispute = {}

    let updateStripeInvoiceDispute = ( params = {} ) => {
      cLog( 'Updating Stripe invoice dispute....', params )

      let body = params.body
      let invoiceId = stripeWebhookInvoiceId( { body: body } )
      let disputeId = stripeWebhookDisputeId( { body: body } )

      return retrieveStripeDispute( { disputeId: disputeId } )
        .then( dispute => {
          if ( dispute instanceof Error ) { return dispute }

          let root = cInvoice.database.path( invoiceId )

          return cDatabase.set( {
            path: `${root}/billing/stripe/dispute/${disputeId}`,
            value: dispute
          } )
            .then( data => {
              if ( data instanceof Error ) {
                cLog( 'Failed to update Stripe invoice dispute:', data )
                return data
              }

              cLog( 'Successfully updated Stripe invoice dispute:', data )
              return { dispute: dispute }
            } )
        } )
    }
    _stripe.invoice.dispute.update = updateStripeInvoiceDispute

    /*
     * Stripe Invoice Refund
     * #stripe-invoice-refund
     */

    _stripe.invoice.refund = {}

    let updateStripeInvoiceRefund = ( params = {} ) => {
      cLog( 'Updating Stripe refunds in invoice....', params )

      let body = params.body
      let invoiceId = stripeWebhookInvoiceId( { body: body } )

      let refundIds = stripeWebhookRefundIds( { body: body } )

      let promises = []
      _.each( refundIds, refundId => { promises.push( retrieveStripeRefund( { refundId: refundId } ) ) } )

      return Promise.all( promises )
        .catch( error => { return error } )
        .then( data => {
          data = cError.parse( data )
          if ( data instanceof Error ) { return data }

          let root = cInvoice.database.path( invoiceId )

          let promises = []
          let refunds = {}
          _.each( data, item => {
            refunds[ item.id ] = item
            promises.push(
              cDatabase.set( {
                path: `${root}/billing/stripe/refund/${item.id}`,
                value: item
              } ) )
          } )
          return Promise.all( promises )
            .catch( error => { return error } )
            .then( data => {
              data = cError.parse( data )
              if ( data instanceof Error ) {
                cLog( 'Failed to update Stripe invoice refunds:', data )
                return data
              }

              cLog( 'Successfully updated Stripe invoice refunds:', data )
              return { refunds: refunds }
            } )
        } )
    }
    _stripe.invoice.refund.update = updateStripeInvoiceRefund

    /*
     * Stripe Price
     * #stripe-price
     * Stripe prices are in cents.
     */

    _stripe.price = {}

    let convertToStripePrice = ( params = {} ) => { return _.round( params.price * 100 ) }
    _stripe.price.convertTo = convertToStripePrice

    let convertFromStripePrice = ( params = {} ) => { return _.round( params.price / 100, 2 ) }
    _stripe.price.convertFrom = convertFromStripePrice

    /*
     * Stripe Refund
     * #stripe-refund
     */

    _stripe.refund = {}

    let createStripeRefund = ( params = {} ) => {
      cLog( 'Creating Stripe refund...', params )

      return stripe.refunds.create( params )
        .then( refund => {
          cLog( 'Successfully created refund with Stripe:', refund )
          return refund
        }, error => {
          cLog( 'Failed to create refund with Stripe:', error )
          return formatStripeError( { error: error } )
        } )
    }
    _stripe.refund.create = createStripeRefund

    let retrieveStripeRefund = ( params = {} ) => {
      cLog( 'Retrieving Stripe refund...', params )

      return stripe.refunds.retrieve( params.refundId )
        .then( refund => {
          cLog( 'Successfully retrieved refund from Stripe:', refund )
          return refund
        }, error => {
          cLog( 'Failed to retrieve refund from Stripe:', error )
          return formatStripeError( { error: error } )
        } )
    }
    _stripe.refund.retrieve = retrieveStripeRefund

    /*
     * Stripe Webhook
     * #stripe-webhook
     */

    _stripe.webhook = {}

    /*
     * Stripe Webhook Charge
     * #stripe-webhook-charge
     */

    _stripe.webhook.charge = {}

    let stripeWebhookChargeId = ( params = {} ) => {
      let data = params.body.data
      let id
      _.each( data, item => {
        if ( item.object === 'charge' ) {
          id = id || item.id
        } else if ( item.charge ) {
          id = id || item.charge
        } else if ( item.originating_transaction ) {
          id = id || item.originating_transaction
        }
      } )
      return id
    }
    _stripe.webhook.charge.id = stripeWebhookChargeId

    /*
     * Stripe Webhook Invoice
     * #stripe-webhook-invoice
     */

    _stripe.webhook.invoice = {}

    // Use charge id as the index for the invoice
    let stripeWebhookInvoiceId = stripeWebhookChargeId
    _stripe.webhook.invoice.id = stripeWebhookInvoiceId

    /*
     * Stripe Webhook Dispute
     * #stripe-webhook-dispute
     */

    _stripe.webhook.dispute = {}

    let stripeWebhookDisputeId = ( params = {} ) => {
      let data = params.body.data
      let id
      _.each( data, item => {
        if ( item.object === 'dispute' ) {
          id = item.id
        } else if ( item.dispute ) {
          // TODO: Not sure that it is possible to have multiple disputes for a charge
          if ( item.dispute.object === 'list' ) {
            _.each( item.dispute.data, dispute => { id = dispute.id } )
          } else if ( item.dispute.object === 'dispute' ) {
            id = item.dispute.id
          }
        }
      } )
      return id
    }
    _stripe.webhook.dispute.id = stripeWebhookDisputeId

    /*
     * Stripe Webhook Refund
     * #stripe-webhook-refund
     */

    _stripe.webhook.refund = {}

    let stripeWebhookRefundIds = ( params = {} ) => {
      let data = params.body.data
      let ids = []
      _.each( data, item => {
        if ( item.object === 'refund' ) {
          ids.push( item.id )
        } else if ( item.refunds ) {
          if ( item.refunds.object === 'list' ) {
            _.each( item.refunds.data, refund => { ids.push( refund.id ) } )
          } else if ( item.refunds.object === 'refund' ) {
            ids.push( item.refunds.id )
          }
        }
      } )
      return _.filter( _.uniq( ids ), id => { return id } )
    }
    _stripe.webhook.refund.ids = stripeWebhookRefundIds

    /*
     * Transaction
     * #transaction
     */

    let _transaction = {}

    let createTransaction = ( params = {} ) => {
      cLog( 'Creating transaction...', params )

      return cCart.fetch( params.uid )
        .then( dataSnapshot => {
          if ( dataSnapshot instanceof Error ) { return dataSnapshot }

          let cart = dataSnapshot.val()
          let transaction = params.data || {}
          transaction.items = cart.items
          transaction.user = params.uid
          transaction.billingId = params.billingId
          transaction.shippingId = params.shippingId
          transaction.processor = params.processor
          transaction.notes = params.notes

          return cDatabase.set( {
            path: transactionPath( params.id ),
            value: transaction
          } )
        } )
    }
    _transaction.create = createTransaction

    let fetchTransaction = id => { return cDatabase.fetch( { path: transactionPath( id ) } ) }
    _transaction.fetch = fetchTransaction

    /*
     * Transaction Database
     * #transaction-database
     */

    _transaction.database = {}

    let transactionPath = id => { return `transaction/${id}` }
    _transaction.database.path = transactionPath

    initialize()

    return {
      config: _config,
      paypal: _paypal,
      report: _report,
      sendgrid: _sendgrid,
      settings: _settings,
      stripe: _stripe,
      transaction: _transaction
    }
  }
}
