module.exports = ( context = {} ) => {
  return ( settings = {} ) => {
    return {
      analytics: () => { return require( './analytics/index' )( context )( settings ) },
      auth: () => { return require( './auth/index' )( context )( settings ) },
      crashlytics: () => { return require( './crashlytics/index' )( context )( settings ) },
      database: () => { return require( './database/index' )( context )( settings ) },
      firestore: () => { return require( './firestore/index' )( context )( settings ) },
      https: () => { return context.functions.https.onRequest( require( './https/index' )( context )( settings ) ) },
      pubsub: () => { return require( './pubsub/index' )( context )( settings ) },
      storage: () => { return require( './storage/index' )( context )( settings ) }
    }
  }
}
