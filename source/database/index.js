const _ = require( 'lodash' )
const moment = require( 'moment' )
require( 'moment-timezone' )
const pdf = require( 'html-pdf' )
const uuid = require( 'uuid/v4' )

module.exports = ( context = {} ) => {
  return ( settings = {} ) => {
    let functions = context.functions

    let core = context.core
    let trigger = context.core.triggers.database

    let cLibs = core.libs
    let cCart = cLibs.cart
    let cDatabase = cLibs.database
    let cError = cLibs.error
    let cInvoice = cLibs.invoice
    let cLog = cLibs.log
    let cPubsub = cLibs.pubsub
    let cUser = cLibs.user

    let cLocalization = cLibs.localization
    let locale = cLocalization.locale()
    let timezone = cLocalization.timezone()

    let libs = context.libs

    let removeMatchingTransaction = ( snap, eventContext ) => {
      return cDatabase.remove( { path: libs.transaction.database.path( snap.key ) } )
    }

    let createStripeCustomer = ( snap, eventContext ) => {
      console.log( 'Event Context:', eventContext )

      let user = snap.val()
      let uid = eventContext.params.uid

      return libs.stripe.customer.create( { email: user.email, metadata: { uid: uid } } )
        .then( customer => {
          if ( customer instanceof Error ) { return customer }

          return cDatabase.set( {
            path: cUser.database.path( uid ) + '/stripe',
            value: customer.id
          } )
        } )
        .then( data => {
          if ( data instanceof Error ) { console.error( 'Could not create customer for user:', data ) }
          return data
        } )
    }

    let createAddressId = ( snap, eventContext ) => {
      console.log( 'Event Context:', eventContext )

      let addressId = eventContext.params.addressId

      return cDatabase.set( {
        path: cUser.database.address.path( {
          addressId: addressId,
          uid: eventContext.params.uid } ) + '/id',
        value: addressId
      } )
    }

    let updateReporting = ( snap, eventContext ) => {
      console.log( 'Event Context:', eventContext )

      let invoice = snap.val()

      cLog( 'Updating reporting for invoice...', invoice )

      let amount = Number( _.result( invoice, 'cost.amount', NaN ) )
      let currencyCode = _.result( invoice, 'cost.currency' )
      let createdAt = _.result( invoice, 'meta.created', moment.tz( timezone ).format() )

      if ( _.isNumber( amount ) && !_.isNaN( amount ) && _.isString( currencyCode ) ) {
        let path = libs.report.database.path( { date: createdAt } ) + '/sales'

        return cDatabase.transaction( {
          path: path,
          func: sales => {
            sales = sales || {}
            sales.count = Number( sales.count || 0 )
            sales.currency = sales.currency || {}
            sales.currency[ currencyCode ] = _.floor( sales.currency[ currencyCode ] || 0, 2 )

            sales.count += 1
            sales.currency[ currencyCode ] += _.floor( amount, 2 )
            sales.currency[ currencyCode ] = _.floor( sales.currency[ currencyCode ], 2 )

            return sales
          }
        } )
          .then( data => {
            if ( data instanceof Error ) { console.error( 'Failed to update aggregate totals in reporting:', data ) }
            return data
          } )
      } else {
        cLog( 'No changes made. Not a valid invoice.' )
        return invoice
      }
    }

    let linkInvoiceToUser = ( snap, eventContext ) => {
      console.log( 'Event Context:', eventContext )

      return cDatabase.fetch( {
        path: cInvoice.database.path( eventContext.params.invoiceId )
      } )
        .then( invoiceSnapshot => {
          if ( invoiceSnapshot instanceof Error ) { return invoiceSnapshot }

          let invoice = invoiceSnapshot.val()

          cLog( 'Creating link for user to invoice...', invoice )

          if ( invoice.user ) {
            return cDatabase.push( {
              path: cUser.database.path( invoice.user ) + '/invoice',
              value: eventContext.params.invoiceId
            } )
              .then( data => {
                if ( data instanceof Error ) { console.error( data ) }
                return data
              } )
          } else {
            cLog( 'No changes made. Not a valid invoice.' )
            return invoice
          }
        } )
    }

    let sendPurchaseReceipt = ( snap, eventContext ) => {
      console.log( 'Event Context:', eventContext )

      let invoiceId = snap.val()
      let uid = eventContext.params.uid

      return Promise.all( [
        cDatabase.fetch( { path: cUser.database.path( uid ) } ),
        cDatabase.fetch( { path: cInvoice.database.path( invoiceId ) } )
      ] )
        .catch( error => { return error } )
        .then( data => {
          data = cError.parse( data )
          if ( data instanceof Error ) { return data }

          let user = data[ 0 ].val()
          let invoice = data[ 1 ].val()

          let hasEmail = ( _.isString( user.email ) || _.isString( user.contactEmail ) )

          if ( !invoice.receipt && hasEmail ) {
            return cPubsub.publish( {
              topic: 'sendgrid_send',
              message: {
                message: {
                  to: user.email || user.contactEmail,
                  from: libs.sendgrid.from(),
                  bcc: settings.DEBUG ? 'accounts@coldandgoji.com' : libs.sendgrid.bcc(),
                  templateId: libs.sendgrid.template.purchaseReceipt(),
                  substitutions: {
                    companyName: libs.sendgrid.companyName(),
                    greeting: user.firstname ? `Hi ${user.firstname},` : '',
                    invoiceUrl: invoice.url,
                    orderNumber: invoice.id,
                    orderTotal: '$' + Number( invoice.cost.amount ).toFixed( 2 )
                  }
                }
              }
            } )
          } else {
            cLog( 'No valid email to send receipt to.' )
            return user
          }
        } )
    }

    let createInvoicePDF = ( snap, eventContext ) => {
      console.log( 'Event Context:', eventContext )

      let core = trigger( snap, eventContext )
      let invoice = snap.val()

      console.log( 'Creating PDF in storage for invoice... v2', invoice )

      let invoiceId = eventContext.params.invoiceId

      if ( invoiceId !== 'meta' && invoiceId !== 'undefined' && invoiceId && invoice.id ) {
        let storage = core.package.admin.storage()

        let filename = `${invoice.id}.pdf`
        let file = storage.bucket().file( `invoice/${invoice.id}/${uuid()}/invoice.pdf` )

        let products = ''
        _.forIn( invoice.items, ( item, key ) => {
          products += '<tr>'
          products += '<td style="padding: 5px 5px 5px 12px; text-align: left; vertical-align: top; border-top: 1px solid #eee;" valign="top" nowrap>'
          products += item.product.modelNumber ? item.product.modelNumber[ locale ] : ''
          products += '</td>'
          products += '<td style="padding: 5px 5px 5px 12px; text-align: left; vertical-align: top; border-top: 1px solid #eee;" valign="top">'
          products += item.product.name[ locale ]
          products += '</td>'
          products += '<td style="padding: 5px 5px 5px 12px; text-align: right; vertical-align: top; border-top: 1px solid #eee;" valign="top" nowrap>'
          products += item.quantity
          products += '</td>'
          products += '<td style="padding: 5px 5px 5px 12px; text-align: right; vertical-align: top; border-top: 1px solid #eee;" valign="top" nowrap>'
          products += '$' + Number( item.price ).toFixed( 2 )
          products += '</td>'
          products += '<td style="padding: 5px 12px 5px 12px; text-align: right; vertical-align: top; border-top: 1px solid #eee;" valign="top" nowrap>'
          products += '$' + ( Number( item.price ) * item.quantity ).toFixed( 2 )
          products += '</td>'
          products += '</tr>'
          _.forIn( item.addons, ( addon, _key ) => {
            products += '<tr>'
            products += '<td style="padding: 5px 5px 5px 12px; text-align: left; vertical-align: top;" valign="top" nowrap>'
            products += addon.product.modelNumber ? addon.product.modelNumber[ locale ] : ''
            products += '</td>'
            products += '<td style="padding: 5px 5px 5px 12px; text-align: left; vertical-align: top;" valign="top">'
            products += addon.product.name[ locale ]
            products += '</td>'
            products += '<td style="padding: 5px 5px 5px 12px; text-align: right; vertical-align: top;" valign="top" nowrap>'
            products += addon.quantity
            products += '</td>'
            products += '<td style="padding: 5px 5px 5px 12px; text-align: right; vertical-align: top;" valign="top" nowrap>'
            products += '$' + Number( addon.price ).toFixed( 2 )
            products += '</td>'
            products += '<td style="padding: 5px 12px 5px 12px; text-align: right; vertical-align: top;" valign="top" nowrap>'
            products += '$' + ( Number( addon.price ) * addon.quantity ).toFixed( 2 )
            products += '</td>'
            products += '</tr>'
          } )
        } )
        products += '<tr>'
        products += '<td style="border-top: 1px solid #eee; padding: 5px 5px 0px 12px; text-align: left; vertical-align: top;" valign="top" nowrap></td>'
        products += '<td style="border-top: 1px solid #eee; padding: 5px 5px 0px 12px; text-align: left; vertical-align: top;" valign="top" nowrap></td>'
        products += '<td style="border-top: 1px solid #eee; padding: 5px 5px 0px 12px; text-align: right; vertical-align: top;" valign="top" nowrap></td>'
        products += '<td style="border-top: 1px solid #eee; padding: 5px 5px 0px 12px; text-align: right; vertical-align: top;" valign="top" nowrap>'
        products += 'Shipping'
        products += '</td>'
        products += '<td style="border-top: 1px solid #eee; padding: 5px 12px 0px 0px; text-align: right; vertical-align: top;" valign="top" nowrap>'
        products += 'FREE'
        products += '</td>'
        products += '</tr>'
        products += '<tr>'
        products += '<td style="padding: 0px 5px 0px 12px; text-align: left; vertical-align: top;" valign="top" nowrap></td>'
        products += '<td style="padding: 0px 5px 0px 12px; text-align: left; vertical-align: top;" valign="top" nowrap></td>'
        products += '<td style="padding: 0px 5px 0px 12px; text-align: right; vertical-align: top;" valign="top" nowrap></td>'
        products += '<td style="padding: 0px 5px 0px 12px; text-align: right; vertical-align: top;" valign="top" nowrap>'
        products += 'GST Included (10%)'
        products += '</td>'
        products += '<td style="padding: 0px 12px 0px 12px; text-align: right; vertical-align: top;" valign="top" nowrap>'
        products += '$' + ( Number( invoice.cost.amount ) / 11 ).toFixed( 2 )
        products += '</td>'
        products += '</tr>'
        products += '<tr>'
        products += '<td style="padding: 0px 5px 0px 12px; text-align: left; vertical-align: top;" valign="top" nowrap></td>'
        products += '<td style="padding: 0px 5px 0px 12px; text-align: left; vertical-align: top;" valign="top" nowrap></td>'
        products += '<td style="padding: 0px 5px 0px 12px; text-align: right; vertical-align: top;" valign="top" nowrap></td>'
        products += '<td style="padding: 0px 5px 0px 12px; text-align: right; vertical-align: top;" valign="top" nowrap>'
        products += '<strong>Total</strong>'
        products += '</td>'
        products += '<td style="padding: 0px 12px 0px 12px; text-align: right; vertical-align: top;" valign="top" nowrap>'
        products += '<strong>$' + Number( invoice.cost.amount ).toFixed( 2 ) + '</strong>'
        products += '</td>'
        products += '</tr>'

        let notes = ''
        if ( invoice.notes && invoice.notes !== 'null' ) {
          notes += '<table cellpadding="0" cellspacing="0" style="line-height: inherit; padding: 24px 0 0 0; text-align: left; width: 100%;" width="100%" align="left">'
          notes += '<tr>'
          notes += '<td style="background: #eee; font-weight: bold; padding: 5px 12px 5px 12px; vertical-align: top;" valign="top">'
          notes += 'Delivery Notes'
          notes += '</td>'
          notes += '</tr>'
          notes += '<tr>'
          notes += '<td style="line-height: 18px; padding: 12px 5px 5px 12px; text-alight: left; vertical-align: top;" valign="top">'
          notes += invoice.notes
          notes += '</td>'
          notes += '</tr>'
          notes += '</table>'
        }

        let html = `
          <!doctype html>
          <html>
          <head>
          <meta charset="utf-8">
          <title>${invoice.id}</title>
          </head>
          <body>
          <div style="color: #555; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 12px; line-height: 24px; margin: auto; max-width: 800px; overflow: auto; padding: 1em;">

          <!-- HEADER -->
          <table cellpadding="0" cellspacing="0" style="line-height: inherit; text-align: left; width: 100%;" width="100%" align="left">
          <tr>
          <td style="background: #555; color: #f5f5f5; font-size: 18px; font-weight: bold; padding: 5px 0px 5px 12px; text-alight: left; text-transform: uppercase; vertical-align: middle;" valign="middle">
          ${invoice.id}
          </td>
          <td style="background: #555; color: #f5f5f5; font-size: 18px; font-weight: bold; padding: 5px 12px 5px 0px; text-align: right; text-transform: uppercase; vertical-align: middle;" valign="middle">
          ${moment.tz( timezone ).format( 'YYYY-MM-DD' )}
          </td>
          </tr>
          </table>
          <!-- /HEADER -->

          <!-- SELLER DETAILS -->
          <table cellpadding="0" cellspacing="0" style="line-height: inherit; text-align: left; width: 100%;" width="100%" align="left">
          <tr>
          <td style="font-size: 10px; line-height: 15px; padding: 12px 5px 5px 12px; text-alight: left; vertical-align: top;" valign="top">
          <strong>Ausclimate Pty Ltd</strong><br />
          ATF Bryan Family Trust<br />
          ABN: 60 667 963 079<br />
          58 Railway St<br />
          WAGGA WAGGA NSW 2650<br />
          Ph: 1800 122 100
          </td>
          <td style="padding:12px 12px 5px 5px; text-align: right; vertical-align: top;" valign="top">
          <img src="https://images.ctfassets.net/0xyjcifbumhw/3HDS5OS1FSK04CCeKE4aKG/d1bdc78fb48cab8f48f433b65f1272ea/logo--ausclimate.png" style="width:100%; max-width:200px;">
          </td>
          </tr>
          </table>
          <!-- /SELLER DETAILS -->

          <!-- BUYER DETAILS -->
          <table cellpadding="0" cellspacing="0" style="line-height: inherit; padding: 24px 0 0 0; text-align: left; width: 100%;table-layout: fixed;" width="100%" align="left">
          <tr>
          <td style="background: #eee; border-right: 5px solid #fff; font-weight: bold; padding: 5px 5px 5px 12px; vertical-align: top;" valign="top">
          Billing Address
          </td>
          <td style="background: #eee; border-left: 5px solid #fff; font-weight: bold; padding: 5px 5px 5px 12px; vertical-align: top;" valign="top">
          Shipping Address
          </td>
          </tr>
          <tr>
          <td style="border-right: 5px solid #fff; line-height: 18px; padding: 12px 5px 5px 12px; text-alight: left; vertical-align: top;" valign="top">
          ${_.values( _.pick( invoice.address.billing, [ 'fullname' ] ) ).join( ' ' )}<br />
          ${_.values( _.pick( invoice.address.billing, [ 'line1', 'line2' ] ) ).join( ' ' )}<br />
          ${_.values( _.pick( invoice.address.billing, [ 'city', 'state', 'postcode' ] ) ).join( ' ' )}<br />
          ${invoice.email ? invoice.email + '<br />' : ''}
        ${invoice.phone || ''}
          </td>
          <td style="border-left: 5px solid #fff; line-height: 18px; padding: 12px 5px 5px 12px; text-alight: left; vertical-align: top;" valign="top">
          ${_.values( _.pick( invoice.address.shipping, [ 'fullname' ] ) ).join( ' ' )}<br />
          ${_.values( _.pick( invoice.address.shipping, [ 'line1', 'line2' ] ) ).join( ' ' )}<br />
          ${_.values( _.pick( invoice.address.shipping, [ 'city', 'state', 'postcode' ] ) ).join( ' ' )}<br />
          </td>
          </tr>
          </table>
          <!-- /BUYER DETAILS -->

          <!-- PRODUCTS -->
          <table cellpadding="0" cellspacing="0" style="line-height: inherit; padding: 24px 0 0 0; text-align: left; width: 100%;" width="100%" align="left">
          <tr>
          <td style="background: #eee; font-weight: bold; padding: 5px 5px 5px 12px; vertical-align: top;" valign="top">
          ID
          </td>
          <td style="background: #eee; font-weight: bold; padding: 5px 5px 5px 12px; vertical-align: top;" valign="top">
          Description
          </td>
          <td style="background: #eee; font-weight: bold; padding: 5px 5px 5px 12px; text-align: right; vertical-align: top;" valign="top">
          Quantity
          </td>
          <td style="background: #eee; font-weight: bold; padding: 5px 5px 5px 12px; text-align: right; vertical-align: top;" valign="top" nowrap>
          Unit Price
          </td>
          <td style="background: #eee; font-weight: bold; padding: 5px 12px 5px 12px; text-align: right; vertical-align: top;" valign="top" nowrap>
          Price
          </td>
          </tr>
          ${products}
          </table>
          <!-- /PRODUCTS -->

          <!-- PAYMENT DETAILS -->
          <table cellpadding="0" cellspacing="0" style="line-height: inherit; padding: 24px 0 0 0; text-align: left; width: 100%;" width="100%" align="left">
          <tr>
          <td style="background: #eee; font-weight: bold; padding: 5px 12px 5px 12px; vertical-align: top;" valign="top">
          Payment Details
          </td>
          </tr>
          <tr>
          <td style="line-height: 18px; padding: 12px 5px 5px 12px; text-alight: left; vertical-align: top;" valign="top">
          ${invoice.processor === 'stripe' ? `Credit Card - ${invoice.card.brand} x${invoice.card.last4} (${invoice.charge.id})` : `PayPal (${invoice.sale.id})`}
          </td>
          </tr>
          </table>
          <!-- /PAYMENT DETAILS -->

          <!-- DELIVERY NOTE -->
          ${notes || ''}
          <!-- /DELIVERY NOTE -->
          </div>
          </body>
          </html> `

        return new Promise( resolve => {
          pdf.create( html ).toBuffer( ( pdfError, pdfBuffer ) => {
            file.save( pdfBuffer, {
              metadata: {
                contentType: 'application/pdf',
                contentDisposition: `inline; filename="${filename}"`,
                metadata: {
                  id: invoice.id,
                  invoice: invoiceId,
                  user: invoice.user
                }
              }
            }, fileError => {
              if ( fileError ) {
                console.error( 'Could not write invoice to bucket:', fileError )
                resolve( fileError )
              } else {
                resolve( 'Successfully wrote invoice to bucket.' )
              }
            } )
          } )
        } )
      } else {
        console.log( 'No changes made. Not a valid invoice.' )
        return invoice
      }
    }

    let pruneInvalidCartItems = ( change, eventContext ) => {
      console.log( 'Event Context:', eventContext )

      // Current cart items
      let items = change.after.val()
      let uid = eventContext.params.uid

      cLog( 'Pruning invalid cart items for:', uid )

      // Product Attribute ID for an "Add On"
      let addonId = settings.product.attribute.addon

      // Cart Item IDs of invalid products
      let invalid = []

      // Fetch all products for cart items
      let promises = []
      _.each( items, item => {
        promises.push( cDatabase.fetch( { path: `content/product/${item.productId}` } ) )
      } )

      return Promise.all( promises )

        .catch( error => { return error } )

        .then( productSnapshots => {
          productSnapshots = cError.parse( productSnapshots )
          if ( productSnapshots instanceof Error ) {
            console.error( 'Could not fetch all products:', productSnapshots )
            return productSnapshots
          }

          // Map products to their items
          _.map( items, item => {
            _.each( productSnapshots, snapshot => {
              if ( snapshot.val().id === item.productId ) { item.product = snapshot.val() }
            } )
            return item
          } )

          // Check to see if items are valid
          _.forIn( items, ( item, itemId ) => {
            let product = item.product
            if ( !product ) {
              // Invalid
              cLog( 'Invalid: Cart item has no correspoding product:', item )
              invalid.push( itemId )
            } else if ( product.attributes ) {
              let attributes = product.attributes[ locale ] || []
              // Product has the "Add On" attribute
              if ( attributes.includes( addonId ) ) {
                // Invalid
                cLog( 'Invalid: Cart item has "Add On" attribute:', item )
                invalid.push( itemId )
              }
            }
          } )

          if ( invalid.length ) {
            // Remove invalid items from cart
            let promises = []

            _.each( invalid, id => {
              promises.push(
                cDatabase.remove( {
                  path: `${cCart.database.path( uid )}/items/${id}`
                } )
              )
            } )

            return Promise.all( promises )
              .catch( error => { return error } )
              .then( data => {
                data = cError.parse( data )
                if ( data instanceof Error ) {
                  console.error( 'Could not remove all invalid cart items:', data )
                  return data
                } else {
                  return 'Pruned invalid cart items.'
                }
              } )
          } else {
            return 'No invalid cart items.'
          }
        } )
    }

    let metaCreated = ( snap, eventContext ) => { return trigger( snap, eventContext ).meta.created() }

    let metaUpdated = ( change, eventContext ) => { return trigger( change, eventContext ).meta.updated() }

    return {
      // Cart
      cartMetaCreated: functions.database.ref( '/cart/{id}' ).onCreate( metaCreated ),
      cartMetaUpdated: functions.database.ref( '/cart/{id}/{field}' ).onUpdate( metaUpdated ),
      pruneInvalidCartItems: functions.database.ref( '/cart/{uid}/items' ).onWrite( pruneInvalidCartItems ),
      // Invoice
      createInvoicePDF: functions.database.ref( '/invoice/{invoiceId}' ).onCreate( createInvoicePDF ),
      removeMatchingTransaction: functions.database.ref( '/invoice/{invoiceId}' ).onCreate( removeMatchingTransaction ),
      invoiceMetaCreated: functions.database.ref( '/invoice/{id}' ).onCreate( metaCreated ),
      invoiceMetaUpdated: functions.database.ref( '/invoice/{id}/{field}' ).onUpdate( metaUpdated ),
      updateReporting: functions.database.ref( '/invoice/{invoiceId}' ).onCreate( updateReporting ),
      linkInvoiceToUser: functions.database.ref( '/invoice/{invoiceId}/url' ).onCreate( linkInvoiceToUser ),
      // User
      createCustomer: functions.database.ref( '/user/{uid}' ).onCreate( createStripeCustomer ),
      createUserAddressId: functions.database.ref( '/user/{uid}/address/{addressId}' ).onCreate( createAddressId ),
      sendPurchaseReceipt: functions.database.ref( '/user/{uid}/invoice/{invoiceKey}' ).onCreate( sendPurchaseReceipt )
    }
  }
}
