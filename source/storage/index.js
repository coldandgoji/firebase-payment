const _ = require( 'lodash' )
const moment = require( 'moment' )
require( 'moment-timezone' )

module.exports = ( context = {} ) => {
  return ( settings = {} ) => {
    let functions = context.functions

    let core = context.core
    let trigger = core.triggers.storage
    let cLibs = core.libs
    let cDatabase = cLibs.database
    let cInvoice = cLibs.invoice
    // let cLog = cLibs.log

    // let libs = context.libs

    /* Deprecated
    let linkInvoiceToUser = ( object, eventContext ) => {
      console.log( 'Event Context:', eventContext )

      let metadata = object.metadata || {}
      let invoiceId = metadata.invoice
      let uid = metadata.user

      // #debug
      cLog( 'Invoice:', invoiceId )
      cLog( 'User:', uid )

      if ( invoiceId && uid ) {
        cLog( 'Creating link for user to invoice...' )

        return cDatabase.push( {
          path: cUser.database.path( uid ) + '/invoice',
          value: invoiceId
        } )
          .then( data => {
            if ( data instanceof Error ) { console.error( data ) }
            return data
          } )
      } else {
        return 'Insufficient metadata to link invoice to user.'
      }
    } */

    let addInvoiceDownloadURL = ( object, eventContext ) => {
      let core = trigger( object, eventContext )

      console.log( 'Object:', object )
      console.log( 'Event Context:', eventContext )

      let metadata = object.metadata || {}
      let tokenString = _.result( metadata, 'firebaseStorageDownloadTokens', '' )
      let isValidInvoice = ( metadata.invoice && metadata.user && metadata.id )

      if ( isValidInvoice && tokenString.length ) {
        let tokens = _.split( tokenString, ',' )

        return cDatabase.set( {
          path: cInvoice.database.path( metadata.invoice ) + '/url',
          value: cInvoice.download.url( {
            fileName: metadata.id,
            projectId: core.libs.project.id(),
            token: tokens[ 0 ]
          } )
        } )
      } else if ( isValidInvoice ) {
        return core.package.admin.storage()
          .bucket()
          .file( object.name )
          .getSignedUrl( { action: 'read', expires: moment().add( 100, 'years' ) } )
          .then( urls => {
            if ( urls instanceof Error ) { return urls }

            return cDatabase.set( {
              path: cInvoice.database.path( metadata.invoice ) + '/url',
              value: urls[ 0 ]
            } )
          } )
      } else {
        return 'Not a valid invoice.'
      }
    }

    return {
      addInvoiceDownloadURL: functions.storage.bucket( context.core.libs.storage.bucket() ).object( 'invoice' ).onFinalize( addInvoiceDownloadURL )
      // linkInvoiceToUser: functions.storage.bucket( context.core.libs.storage.bucket() ).object( 'invoice' ).onFinalize( linkInvoiceToUser )
    }
  }
}
