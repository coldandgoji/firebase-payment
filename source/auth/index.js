module.exports = ( context = {} ) => {
  return ( settings = {} ) => {
    let functions = context.functions

    let core = context.core
    let cLibs = core.libs
    let cCart = cLibs.cart
    let cDatabase = cLibs.database
    let cUser = cLibs.user

    // let libs = context.libs

    let removeCart = ( userRecord, eventContext ) => {
      console.log( 'User Record:', userRecord )
      console.log( 'Event Context:', eventContext )

      return cDatabase.remove( { path: cCart.database.path( userRecord.uid ) } )
    }

    let createAnonymousUser = ( userRecord, eventContext ) => {
      console.log( 'User Record:', userRecord )
      console.log( 'Event Context:', eventContext )

      let uid = userRecord.uid

      if ( !userRecord.email ) {
        return cDatabase.set( {
          path: cUser.database.path( uid ),
          value: { id: uid } } )
      } else {
        return 'No database node created. User is not anonymous.'
      }
    }

    return {
      createAnonymousUser: functions.auth.user().onCreate( createAnonymousUser ),
      removeCart: functions.auth.user().onDelete( removeCart )
    }
  }
}
