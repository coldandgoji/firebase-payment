const _ = require( 'lodash' )
const moment = require( 'moment' )
const pdf = require( 'html-pdf' )
const uuid = require( 'uuid/v4' )
require( 'moment-timezone' )

module.exports = ( context = {} ) => {
  return ( settings = {} ) => {
    let functions = context.functions

    let libs = context.libs
    let core = context.core
    let trigger = context.core.triggers.pubsub

    let cLibs = core.libs
    let cCart = cLibs.cart
    let cDatabase = cLibs.database
    let cError = cLibs.error
    let cInvoice = cLibs.invoice
    let cLog = cLibs.log
    let cPubsub = cLibs.pubsub
    let cUser = cLibs.user

    let cLocalization = cLibs.localization
    let locale = cLocalization.locale()
    let timezone = cLocalization.timezone()

    let createInvoicePDF = ( message, eventContext ) => {
      console.log( 'Event Context:', eventContext )

      let core = trigger( message, eventContext )

      let attributes = message.attributes || {}
      let invoiceId = attributes.invoiceId

      return cDatabase.fetch( {
        path: cInvoice.database.path( invoiceId )
      } )
        .catch( error => { return error } )
        .then( invoiceSnapshot => {
          if ( _.isError( invoiceSnapshot ) ) {
            cLog( 'Failed to fetch invoice.', invoiceSnapshot )
            return message
          }

          let invoice = invoiceSnapshot.val()

          cLog( 'Creating PDF in storage for invoice... v2', invoice )

          if ( invoiceId !== 'meta' && invoiceId !== 'undefined' && invoiceId && invoice.id ) {
            let storage = core.package.admin.storage()

            let filename = `${invoice.id}.pdf`
            let file = storage.bucket().file( `invoice/${invoice.id}/${uuid()}/invoice.pdf` )

            let products = ''
            _.forIn( invoice.items, ( item, key ) => {
              products += '<tr>'
              products += '<td style="padding: 5px 5px 5px 12px; text-align: left; vertical-align: top; border-top: 1px solid #eee;" valign="top" nowrap>'
              products += item.product.modelNumber ? item.product.modelNumber[ locale ] : ''
              products += '</td>'
              products += '<td style="padding: 5px 5px 5px 12px; text-align: left; vertical-align: top; border-top: 1px solid #eee;" valign="top">'
              products += item.product.name[ locale ]
              products += '</td>'
              products += '<td style="padding: 5px 5px 5px 12px; text-align: right; vertical-align: top; border-top: 1px solid #eee;" valign="top" nowrap>'
              products += item.quantity
              products += '</td>'
              products += '<td style="padding: 5px 5px 5px 12px; text-align: right; vertical-align: top; border-top: 1px solid #eee;" valign="top" nowrap>'
              products += '$' + Number( item.price ).toFixed( 2 )
              products += '</td>'
              products += '<td style="padding: 5px 12px 5px 12px; text-align: right; vertical-align: top; border-top: 1px solid #eee;" valign="top" nowrap>'
              products += '$' + ( Number( item.price ) * item.quantity ).toFixed( 2 )
              products += '</td>'
              products += '</tr>'
              _.forIn( item.addons, ( addon, _key ) => {
                products += '<tr>'
                products += '<td style="padding: 5px 5px 5px 12px; text-align: left; vertical-align: top;" valign="top" nowrap>'
                products += addon.product.modelNumber ? addon.product.modelNumber[ locale ] : ''
                products += '</td>'
                products += '<td style="padding: 5px 5px 5px 12px; text-align: left; vertical-align: top;" valign="top">'
                products += addon.product.name[ locale ]
                products += '</td>'
                products += '<td style="padding: 5px 5px 5px 12px; text-align: right; vertical-align: top;" valign="top" nowrap>'
                products += addon.quantity
                products += '</td>'
                products += '<td style="padding: 5px 5px 5px 12px; text-align: right; vertical-align: top;" valign="top" nowrap>'
                products += '$' + Number( addon.price ).toFixed( 2 )
                products += '</td>'
                products += '<td style="padding: 5px 12px 5px 12px; text-align: right; vertical-align: top;" valign="top" nowrap>'
                products += '$' + ( Number( addon.price ) * addon.quantity ).toFixed( 2 )
                products += '</td>'
                products += '</tr>'
              } )
            } )
            products += '<tr>'
            products += '<td style="border-top: 1px solid #eee; padding: 5px 5px 0px 12px; text-align: left; vertical-align: top;" valign="top" nowrap></td>'
            products += '<td style="border-top: 1px solid #eee; padding: 5px 5px 0px 12px; text-align: left; vertical-align: top;" valign="top" nowrap></td>'
            products += '<td style="border-top: 1px solid #eee; padding: 5px 5px 0px 12px; text-align: right; vertical-align: top;" valign="top" nowrap></td>'
            products += '<td style="border-top: 1px solid #eee; padding: 5px 5px 0px 12px; text-align: right; vertical-align: top;" valign="top" nowrap>'
            products += 'Shipping'
            products += '</td>'
            products += '<td style="border-top: 1px solid #eee; padding: 5px 12px 0px 0px; text-align: right; vertical-align: top;" valign="top" nowrap>'
            products += 'FREE'
            products += '</td>'
            products += '</tr>'
            products += '<tr>'
            products += '<td style="padding: 0px 5px 0px 12px; text-align: left; vertical-align: top;" valign="top" nowrap></td>'
            products += '<td style="padding: 0px 5px 0px 12px; text-align: left; vertical-align: top;" valign="top" nowrap></td>'
            products += '<td style="padding: 0px 5px 0px 12px; text-align: right; vertical-align: top;" valign="top" nowrap></td>'
            products += '<td style="padding: 0px 5px 0px 12px; text-align: right; vertical-align: top;" valign="top" nowrap>'
            products += 'GST Included (10%)'
            products += '</td>'
            products += '<td style="padding: 0px 12px 0px 12px; text-align: right; vertical-align: top;" valign="top" nowrap>'
            products += '$' + ( Number( invoice.cost.amount ) / 11 ).toFixed( 2 )
            products += '</td>'
            products += '</tr>'
            products += '<tr>'
            products += '<td style="padding: 0px 5px 0px 12px; text-align: left; vertical-align: top;" valign="top" nowrap></td>'
            products += '<td style="padding: 0px 5px 0px 12px; text-align: left; vertical-align: top;" valign="top" nowrap></td>'
            products += '<td style="padding: 0px 5px 0px 12px; text-align: right; vertical-align: top;" valign="top" nowrap></td>'
            products += '<td style="padding: 0px 5px 0px 12px; text-align: right; vertical-align: top;" valign="top" nowrap>'
            products += '<strong>Total</strong>'
            products += '</td>'
            products += '<td style="padding: 0px 12px 0px 12px; text-align: right; vertical-align: top;" valign="top" nowrap>'
            products += '<strong>$' + Number( invoice.cost.amount ).toFixed( 2 ) + '</strong>'
            products += '</td>'
            products += '</tr>'

            let notes = ''
            if ( invoice.notes && invoice.notes !== 'null' ) {
              notes += '<table cellpadding="0" cellspacing="0" style="line-height: inherit; padding: 24px 0 0 0; text-align: left; width: 100%;" width="100%" align="left">'
              notes += '<tr>'
              notes += '<td style="background: #eee; font-weight: bold; padding: 5px 12px 5px 12px; vertical-align: top;" valign="top">'
              notes += 'Delivery Notes'
              notes += '</td>'
              notes += '</tr>'
              notes += '<tr>'
              notes += '<td style="line-height: 18px; padding: 12px 5px 5px 12px; text-alight: left; vertical-align: top;" valign="top">'
              notes += invoice.notes
              notes += '</td>'
              notes += '</tr>'
              notes += '</table>'
            }

            let html = `
          <!doctype html>
          <html>
          <head>
          <meta charset="utf-8">
          <title>${invoice.id}</title>
          </head>
          <body>
          <div style="color: #555; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 12px; line-height: 24px; margin: auto; max-width: 800px; overflow: auto; padding: 1em;">

          <!-- HEADER -->
          <table cellpadding="0" cellspacing="0" style="line-height: inherit; text-align: left; width: 100%;" width="100%" align="left">
          <tr>
          <td style="background: #555; color: #f5f5f5; font-size: 18px; font-weight: bold; padding: 5px 0px 5px 12px; text-alight: left; text-transform: uppercase; vertical-align: middle;" valign="middle">
          ${invoice.id}
          </td>
          <td style="background: #555; color: #f5f5f5; font-size: 18px; font-weight: bold; padding: 5px 12px 5px 0px; text-align: right; text-transform: uppercase; vertical-align: middle;" valign="middle">
          ${moment.tz( timezone ).format( 'YYYY-MM-DD' )}
          </td>
          </tr>
          </table>
          <!-- /HEADER -->

          <!-- SELLER DETAILS -->
          <table cellpadding="0" cellspacing="0" style="line-height: inherit; text-align: left; width: 100%;" width="100%" align="left">
          <tr>
          <td style="font-size: 10px; line-height: 15px; padding: 12px 5px 5px 12px; text-alight: left; vertical-align: top;" valign="top">
          <strong>Ausclimate Pty Ltd</strong><br />
          ATF Bryan Family Trust<br />
          ABN: 60 667 963 079<br />
          58 Railway St<br />
          WAGGA WAGGA NSW 2650<br />
          Ph: 1800 122 100
          </td>
          <td style="padding:12px 12px 5px 5px; text-align: right; vertical-align: top;" valign="top">
          <img src="https://images.ctfassets.net/0xyjcifbumhw/3HDS5OS1FSK04CCeKE4aKG/d1bdc78fb48cab8f48f433b65f1272ea/logo--ausclimate.png" style="width:100%; max-width:200px;">
          </td>
          </tr>
          </table>
          <!-- /SELLER DETAILS -->

          <!-- BUYER DETAILS -->
          <table cellpadding="0" cellspacing="0" style="line-height: inherit; padding: 24px 0 0 0; text-align: left; width: 100%;table-layout: fixed;" width="100%" align="left">
          <tr>
          <td style="background: #eee; border-right: 5px solid #fff; font-weight: bold; padding: 5px 5px 5px 12px; vertical-align: top;" valign="top">
          Billing Address
          </td>
          <td style="background: #eee; border-left: 5px solid #fff; font-weight: bold; padding: 5px 5px 5px 12px; vertical-align: top;" valign="top">
          Shipping Address
          </td>
          </tr>
          <tr>
          <td style="border-right: 5px solid #fff; line-height: 18px; padding: 12px 5px 5px 12px; text-alight: left; vertical-align: top;" valign="top">
          ${_.values( _.pick( invoice.address.billing, [ 'fullname' ] ) ).join( ' ' )}<br />
          ${_.values( _.pick( invoice.address.billing, [ 'line1', 'line2' ] ) ).join( ' ' )}<br />
          ${_.values( _.pick( invoice.address.billing, [ 'city', 'state', 'postcode' ] ) ).join( ' ' )}<br />
          ${invoice.email ? invoice.email + '<br />' : ''}
        ${invoice.phone || ''}
          </td>
          <td style="border-left: 5px solid #fff; line-height: 18px; padding: 12px 5px 5px 12px; text-alight: left; vertical-align: top;" valign="top">
          ${_.values( _.pick( invoice.address.shipping, [ 'fullname' ] ) ).join( ' ' )}<br />
          ${_.values( _.pick( invoice.address.shipping, [ 'line1', 'line2' ] ) ).join( ' ' )}<br />
          ${_.values( _.pick( invoice.address.shipping, [ 'city', 'state', 'postcode' ] ) ).join( ' ' )}<br />
          </td>
          </tr>
          </table>
          <!-- /BUYER DETAILS -->

          <!-- PRODUCTS -->
          <table cellpadding="0" cellspacing="0" style="line-height: inherit; padding: 24px 0 0 0; text-align: left; width: 100%;" width="100%" align="left">
          <tr>
          <td style="background: #eee; font-weight: bold; padding: 5px 5px 5px 12px; vertical-align: top;" valign="top">
          ID
          </td>
          <td style="background: #eee; font-weight: bold; padding: 5px 5px 5px 12px; vertical-align: top;" valign="top">
          Description
          </td>
          <td style="background: #eee; font-weight: bold; padding: 5px 5px 5px 12px; text-align: right; vertical-align: top;" valign="top">
          Quantity
          </td>
          <td style="background: #eee; font-weight: bold; padding: 5px 5px 5px 12px; text-align: right; vertical-align: top;" valign="top" nowrap>
          Unit Price
          </td>
          <td style="background: #eee; font-weight: bold; padding: 5px 12px 5px 12px; text-align: right; vertical-align: top;" valign="top" nowrap>
          Price
          </td>
          </tr>
          ${products}
          </table>
          <!-- /PRODUCTS -->

          <!-- PAYMENT DETAILS -->
          <table cellpadding="0" cellspacing="0" style="line-height: inherit; padding: 24px 0 0 0; text-align: left; width: 100%;" width="100%" align="left">
          <tr>
          <td style="background: #eee; font-weight: bold; padding: 5px 12px 5px 12px; vertical-align: top;" valign="top">
          Payment Details
          </td>
          </tr>
          <tr>
          <td style="line-height: 18px; padding: 12px 5px 5px 12px; text-alight: left; vertical-align: top;" valign="top">
          ${invoice.processor === 'stripe' ? `Credit Card - ${invoice.card.brand} x${invoice.card.last4} (${invoice.charge.id})` : `PayPal (${invoice.sale.id})`}
          </td>
          </tr>
          </table>
          <!-- /PAYMENT DETAILS -->

          <!-- DELIVERY NOTE -->
          ${notes || ''}
          <!-- /DELIVERY NOTE -->
          </div>
          </body>
          </html> `

            return new Promise( resolve => {
              pdf.create( html ).toBuffer( ( pdfError, pdfBuffer ) => {
                file.save( pdfBuffer, {
                  metadata: {
                    contentType: 'application/pdf',
                    contentDisposition: `inline; filename="${filename}"`,
                    metadata: {
                      id: invoice.id,
                      invoice: invoiceId,
                      user: invoice.user
                    }
                  }
                }, fileError => {
                  if ( fileError ) {
                    console.error( 'Could not write invoice to bucket:', fileError )
                    resolve( fileError )
                  } else {
                    resolve( 'Successfully wrote invoice to bucket.' )
                  }
                } )
              } )
            } )
          } else {
            console.log( 'No changes made. Not a valid invoice.' )
            return invoice
          }
        } )
    }

    let rebuildInvoiceNode = ( message, eventContext ) => {
      console.log( 'Event Context:', eventContext )

      let attributes = message.attributes || {}
      let invoiceId = attributes.invoiceId
      let invoicePath = cInvoice.database.path( invoiceId )

      return cDatabase.fetch( { path: invoicePath } )
        .then( invoiceSnapshot => {
          if ( _.isError( invoiceSnapshot ) ) {
            cLog( 'Failed to fetch invoice.', invoiceSnapshot )
            return message
          }

          let invoice = invoiceSnapshot.val()

          cLog( 'Successfully fetched invoice.', invoice )

          let items = invoice.items || {}

          _.forIn( items, ( item, itemId ) => {
            cLog( 'Fixing item price.', item )

            item.price = cCart.item.price( { item: item } )
            item.totalPrice = item.price

            _.forIn( item.addons || {}, ( addon, addonId ) => {
              addon.price = cCart.item.price( { item: addon } )
              item.totalPrice += addon.price

              item.addons[ addonId ] = addon
            } )

            items[ itemId ] = item
          } )

          cLog( 'Setting invoice items to database...', items )

          return cDatabase.set( {
            path: `${invoicePath}/items`,
            value: items
          } )
        } )
        .then( data => {
          if ( data instanceof Error ) {
            cLog( 'Failed to set invoice items to database.', data )
          } else {
            cLog( 'Successfully set invoice items to database.', data )
          }

          return data
        } )
    }

    let sendPurchaseReceipt = ( message, eventContext ) => {
      console.log( 'Event Context:', eventContext )

      let attributes = message.attributes || {}
      let invoiceId = attributes.invoiceId
      let uid = attributes.uid

      return Promise.all( [
        cDatabase.fetch( { path: cUser.database.path( uid ) } ),
        cDatabase.fetch( { path: cInvoice.database.path( invoiceId ) } )
      ] )
        .catch( error => { return error } )
        .then( data => {
          data = cError.parse( data )
          if ( data instanceof Error ) { return data }

          let user = data[ 0 ].val()
          let invoice = data[ 1 ].val()

          let hasEmail = ( _.isString( user.email ) || _.isString( user.contactEmail ) )

          if ( !invoice.receipt && hasEmail ) {
            return cPubsub.publish( {
              topic: 'sendgrid_send',
              message: {
                message: {
                  to: user.email || user.contactEmail,
                  from: libs.sendgrid.from(),
                  bcc: settings.DEBUG ? 'accounts@coldandgoji.com' : libs.sendgrid.bcc(),
                  templateId: libs.sendgrid.template.purchaseReceipt(),
                  substitutions: {
                    companyName: libs.sendgrid.companyName(),
                    greeting: user.firstname ? `Hi ${user.firstname},` : '',
                    invoiceUrl: invoice.url,
                    orderNumber: invoice.id,
                    orderTotal: '$' + Number( invoice.cost.amount ).toFixed( 2 )
                  }
                }
              }
            } )
          } else {
            cLog( 'No valid email to send receipt to.' )
            return user
          }
        } )
    }

    return {
      createInvoicePDF: functions.pubsub.topic( 'create_invoice_pdf' ).onPublish( createInvoicePDF ),
      rebuildInvoiceNode: functions.pubsub.topic( 'rebuild_invoice_node' ).onPublish( rebuildInvoiceNode ),
      sendPurchaseReceipt: functions.pubsub.topic( 'send_purchase_receipt' ).onPublish( sendPurchaseReceipt )
    }
  }
}
