const _ = require( 'lodash' )
const express = require( 'express' )
const stringify = require( 'json-stringify-safe' )
const VError = require( 'verror' )

module.exports = ( context = {} ) => {
  return ( settings = {} ) => {
    return ( request, response ) => {
      // Initialize core libraries
      let core = context.core.triggers.https( request, response )

      // Expose core libraries
      let cLibs = core.libs
      let cCart = cLibs.cart
      // let cDatabase = cLibs.database
      let cError = cLibs.error
      let cLog = cLibs.log

      // Set response headers
      core.response.header( 'Access-Control-Allow-Methods', 'GET,PATCH,POST,PUT,DELETE' )
      core.response.header( 'Access-Control-Allow-Headers', 'Content-Type, Authorization' )
      core.response.header( 'Access-Control-Allow-Origin', '*' )
      core.response.header( 'Content-Type', 'application/json' )

      // Expose request headers
      let authorization = request.get( 'Authorization' )
      let contentType = request.get( 'Content-Type' )

      // Log request data
      if ( request.get( 'Origin' ) ) { cLog( 'Origin:', request.get( 'Origin' ) ) }
      if ( request.path && request.method ) { cLog( request.method, request.path ) }
      if ( _.keys( request.body ).length ) { cLog( 'Body:', stringify( request.body ) ) }
      if ( _.keys( request.query ).length ) { cLog( 'Query:', stringify( request.query ) ) }
      if ( authorization ) { cLog( 'Authorization:', authorization ) }
      if ( contentType ) { cLog( 'Content-Type:', contentType ) }

      // Expose request body
      let body = request.body || {}

      // Expose local libraries
      let libs = context.libs

      /*
       * Invoice
       * #invoice
       */

      // Not implementing for v1
      /* let postInvoiceRefund = ( request, response, next ) => {
        let amount = body.amount
        let invoiceId = request.params.invoiceId
        let invoicePath = cInvoice.database.path( invoiceId )

        return Promise.all( [
          core.authorize.bearerWithData(),
          cDatabase.fetch( { path: invoicePath } )
        ] )

          .catch( error => { return error } )

          .then( data => {
            data = cError.parse( data )
            if ( data instanceof Error ) { return data }

            // Issue refund if user has sufficient claims
            let userData = data[ 0 ].userData
            let invoice = data[ 1 ].val()

            let claims = userData.claims || {}
            let isAdmin = ( claims.admin === true )
            let isEditor = ( claims.editor === true )
            let isOwner = ( claims.owner === true )

            if ( isAdmin || isEditor || isOwner ) {
              switch ( invoice.processor ) {
                case 'paypal':
                  // TODO: Refactor.
                  // This needs to be handled differently than the Stripe case.
                  // This is because Paypal resources related to the payment need to be parsed to determine what action to take.
                  return libs.paypal.refund.issue( {
                    amount: amount,
                    invoice: invoice,
                    invoicePath: invoicePath
                  } )
                case 'stripe':
                  return libs.stripe.refund.create( {
                    amount: amount,
                    // TODO: Think about putting this in the invoice
                    charge: data[ 1 ].key,
                    metadata: body.metadata,
                    reason: body.reason
                  } )
                default:
                  return cError.create( {
                    error: new VError( { name: 'issue-refund' }, 'There is no valid billing source to refund.' ),
                    info: {
                      https: {
                        code: 404,
                        response: {
                          message: 'There is no valid billing source to refund.',
                          status: 'billing/not-found'
                        }
                      }
                    }
                  } )
              }
            } else {
              // Return standard 401 error
              return cError.create( {
                error: new VError( { name: 'verify-claims' }, 'User does not have permission to issue a refund.' ),
                info: { https: { code: 401 } }
              } )
            }
          } )

        // Respond to caller
          .then( refund => {
            if ( refund instanceof Error ) {
              return core.end( refund )
            } else {
              return core.end( {
                code: 200,
                response: {
                  body: refund,
                  message: 'Successfully issued refund.',
                  status: 'success'
                }
              } )
            }
          } )
      } */

      /*
       * Paypal
       * #paypal
       */

      let postPaypalCheckoutCreate = ( request, response, next ) => {
        let payment
        let uid

        return core.authorize.bearer( { token: body.idToken } )
          .then( data => {
            if ( data instanceof Error ) { return data }

            uid = _.result( data, 'decodedIdToken.uid', '' )

            return libs.paypal.payment.create( {
              notes: body.notes,
              billingId: body.billingId,
              shippingId: body.shippingId,
              uid: uid
            } )
          } )
          .then( data => {
            if ( data instanceof Error ) { return data }

            payment = data

            return libs.transaction.create( {
              processor: 'paypal',
              uid: uid,
              id: payment.id,
              billingId: body.billingId,
              shippingId: body.shippingId,
              notes: body.notes,
              data: { payment: payment }
            } )
          } )
          .then( data => {
            if ( data instanceof Error ) { return core.end( data ) }

            return core.end( {
              code: 200,
              response: {
                body: payment,
                message: 'Successfully created payment with Paypal.',
                status: 'success'
              }
            } )
          } )
      }

      let postPaypalCheckoutExecute = ( request, response, next ) => {
        return libs.paypal.payment.execute( { payerID: body.payerID, paymentID: body.paymentID } )
          .then( payment => {
            if ( payment instanceof Error ) { return core.end( payment ) }

            return libs.paypal.invoice.create( { invoiceId: body.paymentID, payment: payment } )
              .then( invoice => {
                if ( invoice instanceof Error ) {
                  cLog( 'Error creating invoice for successful PayPal transaction.' )
                  invoice = {}
                } else {
                  try {
                    console.log( 'Successfully executed payment with Paypal.' )
                    console.log( 'Invoice ID:', invoice.id )
                    console.log( 'Payment ID:', payment.id )
                  } catch ( error ) {
                    console.log( 'Problem logging transaction details.' )
                  }
                }

                return core.end( {
                  code: 200,
                  response: {
                    body: { payment: payment, invoice: invoice },
                    message: 'Successfully executed payment with Paypal.',
                    status: 'success'
                  }
                } )
              } )
          } )
      }

      let postPaypalWebhook = ( request, response, next ) => {
        let accessToken

        return libs.paypal.accessToken.fetch()

          .then( data => {
            if ( data instanceof Error ) { return new VError( data, 'Could not verifiy webhook' ) }

            accessToken = data

            return libs.paypal.webhook.verify( {
              accessToken: accessToken,
              body: body,
              auth_algo: request.get( 'PAYPAL-AUTH-ALGO' ),
              cert_url: request.get( 'PAYPAL-CERT-URL' ),
              transmission_id: request.get( 'PAYPAL-TRANSMISSION-ID' ),
              transmission_sig: request.get( 'PAYPAL-TRANSMISSION-SIG' ),
              transmission_time: request.get( 'PAYPAL-TRANSMISSION-TIME' )
            } )
          } )

          .then( data => {
            if ( data instanceof Error ) { return new VError( data, 'Could not handle webhook' ) }

            // let eventType = body.event_type;
            let resourceParams = {
              accessToken: accessToken,
              resource: body.resource,
              resourceType: body.resource_type
            }

            // Create invoice on completed sale
            // Update invoice for all other event types that are handled
            switch ( body.event_type ) {
              // Capture
              case 'PAYMENT.CAPTURE.COMPLETED':
                return libs.paypal.resources.fetch( resourceParams )
                  .then( resources => {
                    if ( resources instanceof Error ) { return resources }
                    resourceParams.resources = resources
                    return libs.paypal.invoice.update( resourceParams )
                  } )
              case 'PAYMENT.CAPTURE.DENIED':
                return libs.paypal.resources.fetch( resourceParams )
                  .then( resources => {
                    if ( resources instanceof Error ) { return resources }
                    resourceParams.resources = resources
                    return libs.paypal.invoice.update( resourceParams )
                  } )
              case 'PAYMENT.CAPTURE.PENDING':
                return libs.paypal.resources.fetch( resourceParams )
                  .then( resources => {
                    if ( resources instanceof Error ) { return resources }
                    resourceParams.resources = resources
                    return libs.paypal.invoice.update( resourceParams )
                  } )
              case 'PAYMENT.CAPTURE.REFUNDED':
                return libs.paypal.resources.fetch( resourceParams )
                  .then( resources => {
                    if ( resources instanceof Error ) { return resources }
                    resourceParams.resources = resources
                    return libs.paypal.invoice.update( resourceParams )
                  } )
              case 'PAYMENT.CAPTURE.REVERSED':
                return libs.paypal.resources.fetch( resourceParams )
                  .then( resources => {
                    if ( resources instanceof Error ) { return resources }
                    resourceParams.resources = resources
                    return libs.paypal.invoice.update( resourceParams )
                  } )
                // Sale
              case 'PAYMENT.SALE.COMPLETED':
                return libs.paypal.resources.fetch( resourceParams )
                  .then( resources => {
                    if ( resources instanceof Error ) { return resources }
                    resourceParams.resources = resources
                    // #refactor - creating invoice on sucessful payment execution
                    // return libs.paypal.invoice.create( resourceParams );
                    return libs.paypal.invoice.update( resourceParams )
                  } )
              case 'PAYMENT.SALE.PENDING':
                return libs.paypal.resources.fetch( resourceParams )
                  .then( resources => {
                    if ( resources instanceof Error ) { return resources }
                    resourceParams.resources = resources
                    return libs.paypal.invoice.update( resourceParams )
                  } )
              case 'PAYMENT.SALE.REFUNDED':
                return libs.paypal.resources.fetch( resourceParams )
                  .then( resources => {
                    if ( resources instanceof Error ) { return resources }
                    resourceParams.resources = resources
                    return libs.paypal.invoice.update( resourceParams )
                  } )
              case 'PAYMENT.SALE.REVERSED':
                return libs.paypal.resources.fetch( resourceParams )
                  .then( resources => {
                    if ( resources instanceof Error ) { return resources }
                    resourceParams.resources = resources
                    return libs.paypal.invoice.update( resourceParams )
                  } )
                // Default
              default:
                return Promise.resolve( 'Unhandled event type:', body.event_type )
            }
          } )

          .then( data => {
            if ( data instanceof Error ) { return core.end( data ) }

            return core.end( {
              code: 200,
              response: {
                body: data,
                message: 'Successfully handled webhook.',
                status: 'success'
              }
            } )
          } )
      }

      let postPaypalExperience = ( request, response, next ) => {
        return core.authorize.bearerWithData()
          .then( data => {
            if ( data instanceof Error ) { return data }

            let claims = _.result( data, 'userData.claims', {} )

            cLog( 'Checking claims...', claims )

            if ( claims.admin === true || claims.owner === true ) {
              cLog( 'User has sufficient claims.' )
              return libs.paypal.experience.create( body.experience )
            } else {
              cLog( 'User does not have sufficient claims.' )
              return cError.create( { https: { code: 401 } } )
            }
          } )
          .then( data => {
            if ( data instanceof Error ) { return core.end( data ) }

            return core.end( {
              code: 200,
              response: {
                body: data,
                message: 'Experience created for Paypal.',
                status: 'success'
              }
            } )
          } )
      }

      let listPaypalExperience = ( request, response, next ) => {
        return core.authorize.bearerWithData()
          .then( data => {
            if ( data instanceof Error ) { return data }

            let claims = _.result( data, 'userData.claims', {} )

            cLog( 'Checking claims...', claims )

            if ( claims.admin === true || claims.owner === true ) {
              cLog( 'User has sufficient claims.' )
              return libs.paypal.experience.list()
            } else {
              cLog( 'User does not have sufficient claims.' )
              return cError.create( { https: { code: 401 } } )
            }
          } )
          .then( data => {
            if ( data instanceof Error ) { return core.end( data ) }

            return core.end( {
              code: 200,
              response: {
                body: { list: data },
                message: 'Experience list retrieved from Paypal.',
                status: 'success'
              }
            } )
          } )
      }

      let fetchPaypalExperience = ( request, response, next ) => {
        return core.authorize.bearerWithData()
          .then( data => {
            if ( data instanceof Error ) { return data }

            let claims = _.result( data, 'userData.claims', {} )

            cLog( 'Checking claims...', claims )

            if ( claims.admin === true || claims.owner === true ) {
              cLog( 'User has sufficient claims.' )
              return libs.paypal.experience.fetch( request.params )
            } else {
              cLog( 'User does not have sufficient claims.' )
              return cError.create( { https: { code: 401 } } )
            }
          } )
          .then( data => {
            if ( data instanceof Error ) { return core.end( data ) }

            return core.end( {
              code: 200,
              response: {
                body: { profile: data },
                message: 'Experience retrieved from Paypal.',
                status: 'success'
              }
            } )
          } )
      }

      let deletePaypalExperience = ( request, response, next ) => {
        return core.authorize.bearerWithData()
          .then( data => {
            if ( data instanceof Error ) { return data }

            let claims = _.result( data, 'userData.claims', {} )

            cLog( 'Checking claims...', claims )

            if ( claims.admin === true || claims.owner === true ) {
              cLog( 'User has sufficient claims.' )
              return libs.paypal.experience.delete( request.params )
            } else {
              cLog( 'User does not have sufficient claims.' )
              return cError.create( { https: { code: 401 } } )
            }
          } )
          .then( data => {
            if ( data instanceof Error ) { return core.end( data ) }

            return core.end( {
              code: 200,
              response: {
                body: { profile: data },
                message: 'Experience deleted from Paypal.',
                status: 'success'
              }
            } )
          } )
      }

      let patchPaypalExperience = ( request, response, next ) => {
        return core.authorize.bearerWithData()
          .then( data => {
            if ( data instanceof Error ) { return data }

            let claims = _.result( data, 'userData.claims', {} )

            cLog( 'Checking claims...', claims )

            if ( claims.admin === true || claims.owner === true ) {
              cLog( 'User has sufficient claims.' )
              return libs.paypal.experience.patch( _.assign( body.experience, request.params ) )
            } else {
              cLog( 'User does not have sufficient claims.' )
              return cError.create( { https: { code: 401 } } )
            }
          } )
          .then( data => {
            if ( data instanceof Error ) { return core.end( data ) }

            return core.end( {
              code: 204,
              response: {
                message: 'Experience patched for Paypal.',
                status: 'success'
              }
            } )
          } )
      }

      let putPaypalExperience = ( request, response, next ) => {
        return core.authorize.bearerWithData()
          .then( data => {
            if ( data instanceof Error ) { return data }

            let claims = _.result( data, 'userData.claims', {} )

            cLog( 'Checking claims...', claims )

            if ( claims.admin === true || claims.owner === true ) {
              cLog( 'User has sufficient claims.' )
              return libs.paypal.experience.overwrite( _.assign( body.experience, request.params ) )
            } else {
              cLog( 'User does not have sufficient claims.' )
              return cError.create( { https: { code: 401 } } )
            }
          } )
          .then( data => {
            if ( data instanceof Error ) { return core.end( data ) }

            return core.end( {
              code: 204,
              response: {
                message: 'Experience overwritten for Paypal.',
                status: 'success'
              }
            } )
          } )
      }

      /*
       * Stripe
       * #stripe
       */

      let postStripeCheckout = ( request, response, next ) => {
        let source = body.source
        let cost = body.cost
        let uid
        let customerId
        let items

        return core.authorize.bearerWithData()

          .then( data => {
            if ( data instanceof Error ) { return data }

            uid = _.result( data, 'decodedIdToken.uid', '' )
            customerId = _.result( data, 'userData.stripe', '' )

            return cCart.items.validateCost( { cost: cost, uid: uid } )
          } )

          .then( data => {
            if ( data instanceof Error ) { return data }

            items = data.formattedItems

            return libs.stripe.charge.create( {
              amount: cost.amount,
              source: source,
              customerId: customerId
            } )
          } )

          .then( charge => {
            if ( charge instanceof Error ) { return core.end( charge ) }

            // Do not return errors after this point.
            // If charge has gone through, report a success.

            return libs.stripe.invoice.create( {
              source: source,
              charge: charge,
              customerId: customerId,
              items: items,
              billingId: body.billing.id,
              shippingId: body.shipping.id,
              uid: uid,
              notes: body.notes
            } )
              .then( invoice => {
                if ( invoice instanceof Error ) {
                  cLog( 'Error creating invoice for successful Stripe transaction.' )
                  invoice = {}
                } else {
                  try {
                    console.log( 'Successfully created charge with Stripe.' )
                    console.log( 'Invoice ID:', invoice.id )
                    console.log( 'Charge ID:', charge.id )
                  } catch ( error ) {
                    console.log( 'Problem logging transaction details.' )
                  }
                }

                return core.end( {
                  code: 200,
                  response: {
                    body: { charge: charge, invoice: invoice },
                    message: 'Successfully created charge with Stripe.',
                    status: 'success'
                  }
                } )
              } )
          } )
      }

      let getStripeCustomer = ( request, response, next ) => {
        return core.authorize.bearerWithData()
          .then( data => {
            if ( data instanceof Error ) { return data }

            let uid = _.result( data, 'decodedIdToken.uid', '' )
            let userData = data.userData || {}

            if ( !userData.stripe ) {
              cLog( 'No Stripe customer account found. Creating account...' )

              return libs.stripe.customer.createForUser( {
                email: userData.email,
                uid: uid
              } )
            } else {
              cLog( 'Stripe customer account found. Retrieving data...' )

              return libs.stripe.customer.retrieve( { user: userData } )
                .then( customer => {
                  if ( customer instanceof Error ) {
                    let errorName = customer.cause().name
                    let httpsError = cError.https.create( customer )

                    cLog( `Attempting to rescue for error... (name: ${errorName}, code: ${httpsError.code})` )

                    switch ( errorName ) {
                      case 'StripeInvalidRequestError':
                        // Only creating new Stripe customer if a user could not be found
                        if ( httpsError.code === 404 ) {
                          return libs.stripe.customer.createForUser( {
                            email: userData.email,
                            uid: uid
                          } )
                        } else {
                          return customer
                        }
                      default:
                        return customer
                    }
                  }
                  return customer
                } )
            }
          } )
          .then( customer => {
            if ( customer instanceof Error ) { return core.end( customer ) }

            return core.end( {
              code: 200,
              response: {
                body: { customer: customer },
                message: 'Successfully retrieved customer.',
                status: 'success'
              }
            } )
          } )
      }

      let postStripeCustomer = ( request, response, next ) => {
        return core.authorize.bearerWithData()
          .then( data => {
            if ( data instanceof Error ) { return data }

            return libs.stripe.customer.update( { user: data.userData, body: body } )
          } )
          .then( customer => {
            if ( customer instanceof Error ) { return core.end( customer ) }

            return core.end( {
              code: 200,
              response: {
                body: { customer: customer },
                message: 'Successfully updated customer.',
                status: 'success'
              }
            } )
          } )
      }

      let deleteStripeCustomerSource = ( request, response, next ) => {
        return core.authorize.bearerWithData()
          .then( data => {
            if ( data instanceof Error ) { return data }

            return libs.stripe.customer.source.delete( { user: data.userData, sourceId: body.sourceId } )
          } )
          .then( source => {
            if ( source instanceof Error ) { return core.end( source ) }

            return core.end( {
              code: 201,
              response: {
                message: 'Successfully detatched source from customer.',
                status: 'success'
              }
            } )
          } )
      }

      let postStripeCustomerSource = ( request, response, next ) => {
        return core.authorize.bearerWithData()
          .then( data => {
            if ( data instanceof Error ) { return data }

            return libs.stripe.customer.source.create( { user: data.userData, body: body } )
          } )
          .then( source => {
            if ( source instanceof Error ) { return core.end( source ) }

            return core.end( {
              code: 200,
              response: {
                body: { source: source },
                message: 'Successfully updated customer.',
                status: 'success'
              }
            } )
          } )
      }

      let postStripeWebhook = ( request, response, next ) => {
        let webhookType = body.type

        cLog( 'Processing event:', webhookType )

        // Extract object ids from webhook event.
        // These will be undefined if not found.
        let chargeId = libs.stripe.webhook.charge.id( { body: body } )
        let disputeId = libs.stripe.webhook.dispute.id( { body: body } )
        let refundIds = libs.stripe.webhook.refund.ids( { body: body } )

        // Using `Promise.all()` here for everything to get a uniform response
        // Always fetching charge
        let promises = [ libs.stripe.charge.retrieve( { chargeId: chargeId } ) ]

        return new Promise( ( resolve, reject ) => {
          switch ( webhookType ) {
            case 'charge.captured':
              resolve( Promise.all( promises ) )
              break
            case 'charge.dispute.closed':
              promises.push( libs.stripe.dispute.retrieve( { disputeId: disputeId } ) )
              resolve( Promise.all( promises ) )
              break
            case 'charge.dispute.created':
              promises.push( libs.stripe.dispute.retrieve( { disputeId: disputeId } ) )
              resolve( Promise.all( promises ) )
              break
            case 'charge.dispute.funds_reinstated':
              promises.push( libs.stripe.dispute.retrieve( { disputeId: disputeId } ) )
              resolve( Promise.all( promises ) )
              break
            case 'charge.dispute.funds_withdrawn':
              promises.push( libs.stripe.dispute.retrieve( { disputeId: disputeId } ) )
              resolve( Promise.all( promises ) )
              break
            case 'charge.dispute.retrieved':
              promises.push( libs.stripe.dispute.retrieve( { disputeId: disputeId } ) )
              resolve( Promise.all( promises ) )
              break
            case 'charge.failed':
              resolve( Promise.all( promises ) )
              break
            case 'charge.pending':
              resolve( Promise.all( promises ) )
              break
            case 'charge.refund.retrieved':
              _.each( refundIds, refundId => { promises.push( libs.stripe.refund.retrieve( { refundId: refundId } ) ) } )
              resolve( Promise.all( promises ) )
              break
            case 'charge.refunded':
              _.each( refundIds, refundId => { promises.push( libs.stripe.refund.retrieve( { refundId: refundId } ) ) } )
              resolve( Promise.all( promises ) )
              break
            case 'charge.succeeded':
              resolve( Promise.all( promises ) )
              break
            case 'charge.retrieved':
              resolve( Promise.all( promises ) )
              break
            default:
              resolve( new VError( 'Unhandled event type:', webhookType ) )
              break
          }
        } )

          .catch( error => { return error } )

          .then( data => {
            data = cError.parse( data )
            if ( data instanceof Error ) { return data }

            let resources = {}
            _.each( data, item => {
              // Indexing item by object type ( i.e. 'dispute', 'charge' )
              if ( item.object === 'refund' ) {
                // Can have multiple refunds. Indexing `refund` as `refunds[ id ]`
                resources.refunds = resources.refunds || {}
                resources.refunds[ item.id ] = item
              } else {
                resources[ item.object ] = item
              }
            } )

            return libs.stripe.invoice.status.update( { resources: resources } )
          } )

          .then( data => {
            if ( data instanceof Error ) { return core.end( data ) }

            return core.end( {
              code: 201,
              response: {
                message: 'Successfully handled webhook.',
                status: 'success'
              }
            } )
          } )
      }

      /*
       * User
       * #user
       */
      let postUserRegister = ( request, response, next ) => {
        let userParams = {
          auth: {
            email: body.email,
            password: body.password
          },
          database: {
            email: body.email,
            firstname: body.firstname,
            lastname: body.lastname
          }
        }

        return core.user.create( userParams )
          .then( user => {
            if ( user instanceof Error ) { return core.end( user ) }

            return core.end( {
              code: 200,
              response: {
                status: 'success',
                message: 'Created payment user.',
                body: { user: user }
              }
            } )
          } )
      }

      /*
       * Endpoints
       * #endpoints
       */

      // Compose express app
      let app = express()

      app.post( '/paypal/checkout/create', postPaypalCheckoutCreate )
      app.post( '/paypal/checkout/execute', postPaypalCheckoutExecute )
      app.post( '/paypal/experience', postPaypalExperience )
      app.get( '/paypal/experience', listPaypalExperience )
      app.delete( '/paypal/experience/:profileId', deletePaypalExperience )
      app.get( '/paypal/experience/:profileId', fetchPaypalExperience )
      app.patch( '/paypal/experience/:profileId', patchPaypalExperience )
      app.put( '/paypal/experience/:profileId', putPaypalExperience )
      app.post( '/paypal/webhook', postPaypalWebhook )
      app.post( '/stripe/checkout', postStripeCheckout )
      app.get( '/stripe/customer', getStripeCustomer )
      app.post( '/stripe/customer', postStripeCustomer )
      app.delete( '/stripe/customer/source', deleteStripeCustomerSource )
      app.post( '/stripe/customer/source', postStripeCustomerSource )
      app.post( '/stripe/webhook', postStripeWebhook )
      app.post( '/user/register', postUserRegister )

      // Not implementing for v1.
      // app.post( '/invoice/:invoiceId/refund', postInvoiceRefund );

      return app( request, response )
    }
  }
}
